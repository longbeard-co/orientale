<?php
$bridge_qode_options = bridge_qode_return_global_options();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <?php
  $bridge_qode_is_IE = bridge_qode_return_is_ie_variable();

  if (!empty($bridge_qode_is_IE) && $bridge_qode_is_IE) { ?>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <?php } ?>

  <?php
  /**
   * bridge_qode_header_meta hook
   *
   * @see bridge_core_header_meta() - hooked with 10
   * @see bridge_qode_user_scalable_meta() - hooked with 10
   */
  do_action('bridge_qode_action_header_meta');
  ?>

  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage" style="background-color:rgba(255, 255, 255, 1);">


  <div class="wrapper" style="background-color:rgba(255, 255, 255, 1);">
    <div class="wrapper_inner">
      <div class="maintenance" style="padding: 120px 8.333vw">
        <img src="https://orientale.it/en/wp-content/uploads/2016/10/redLogoSVG.svg" alt="Orientale" width="100" style="margin-bottom:1.5rem;" />
        <h1 style="color:#660b21;margin-bottom:1.5rem;">Our website is under maintenance.</h1>
        <p style="color:#333;">Apologies for your inconvenience. Please check back soon.</p>
      </div>
    </div>
  </div>
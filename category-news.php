<?php get_header(); ?>
<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();

$bridge_qode_id = bridge_qode_get_page_id();

if (get_query_var('paged')) {
	$bridge_qode_paged = get_query_var('paged');
} elseif (get_query_var('page')) {
	$bridge_qode_paged = get_query_var('page');
} else {
	$bridge_qode_paged = 1;
}

$bridge_qode_sidebar = $bridge_qode_options['category_blog_sidebar'];

?>
<?php get_template_part('title'); ?>
<div class="container">
	<h1>
		<?php $lang = get_locale();
		if ($lang == 'en_US') {
			echo ($wp_query->query_vars['category_name'] == 'news') ? 'News & Events' : '';
		} else {
			echo ($wp_query->query_vars['category_name'] == 'news') ? 'Notizie & Eventi' : '';
		}
		?>
		<?php echo $wp_query->query_vars['cat'] && $wp_query->query_vars['year'] ? '- ' : ''; ?>
		<?php echo $wp_query->query_vars['year'] ? $wp_query->query_vars['year'] : ''; ?>
	</h1>
	<?php if (isset($bridge_qode_options['overlapping_content']) && $bridge_qode_options['overlapping_content'] == 'yes') { ?>
		<div class="overlapping_content">
			<div class="overlapping_content_inner">
			<?php } ?>
			<div class="container_inner default_template_holder clearfix">
				<div class="vc_row wpb_row section vc_row-fluid  grid_section" style=" text-align:left;">
					<div class=" section_inner clearfix">
						<div class="section_inner_margin clearfix">
							<div class="wpb_column vc_column_container vc_col-sm-12">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<?php
										$columns_number = '';
										$featured_image_size = '';
										$image_width = '';
										$image_height = '';
										$text_length = '';

										$args = array(
											'orderby' => 'date',
											'order' => 'DESC',
											'posts_per_page' => 1,
											//'offset' => $a['numToSkip'],
											'date_query' => array(
												array(
													'year'  => $wp_query->query_vars['year']
												),
											),
											// 'cat' => $wp_query->query_vars['cat'],
										);
										if ($wp_query->query_vars['category_name'] == 'news') :
											$args['category_name'] = 'news,events';
										endif;

										$q = new WP_Query($args);

										$html = "";
										$html .= "<div class='latest_post_holder image_in_box $columns_number'>";
										$html .= "<ul>";

										while ($q->have_posts()) : $q->the_post();

											$cat = get_the_category();

											$html .= '<li class="clearfix">';
											$html .= '<div class="latest_post">';
											$html .= '<div class="latest_post_image clearfix">';

											if ($featured_image_size !== 'custom' || $image_width == '' || $image_height == '') {
												$html .= get_the_post_thumbnail(get_the_ID(), 'large');
											} else {
												$html .= qode_generate_thumbnail(get_post_thumbnail_id(get_the_ID()), null, $image_width, $image_height);
											}

											$html .= '</div>';
											$html .= '<div class="latest_post_text"><div class="latest_post_inner"><div class="latest_post_text_inner">';
											$html .= '<h2 itemprop="name" class="latest_post_title entry_title"><a itemprop="url" href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
											$html .= '<div class="post_infos">';

											if ($text_length != '0') {
												$excerpt = ($text_length > 0) ? mb_substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt();
												$html .= '<span class="date_hour_holder"><div itemprop="dateCreated" class="post_info_date entry_date updated">' . get_the_time('d M Y') . '</div></span>';
											}

											$html .= '<a class="category-link" itemprop="url" href="' . get_category_link($cat[0]->term_id) . '">' . $cat[0]->cat_name . '</a>';
											$html .= '</div></div></div></div></div>';
											$html .= '</li>';

										endwhile;

										$html .= "</ul></div>";

										echo $html;
										wp_reset_postdata();
										?>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="vc_row wpb_row section vc_row-fluid  grid_section" style=" text-align:left;">
					<div class=" section_inner clearfix">
						<div class="section_inner_margin clearfix">
							<div class="wpb_column vc_column_container vc_col-sm-8">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<?php
										$display_featured_images = false;

										$args = array(
											'orderby' => 'date',
											'order' => 'DESC',
											'posts_per_page' => 9999,
											'offset' => 1,
											'date_query' => array(
												array(
													'year'  => $wp_query->query_vars['year']
												),
											),
											// 'cat' => $wp_query->query_vars['cat'],
										);
										if ($wp_query->query_vars['category_name'] == 'news') :
											$args['category_name'] = 'news,events';
										endif;

										$q2 = new WP_Query($args);


										$html = "";
										$html .= "<div class='latest_post_holder minimal $columns_number'>";
										$html .= "<ul>";

										while ($q2->have_posts()) : $q2->the_post();

											$cat = get_the_category();

											$html .= '<li class="clearfix">';
											if ($display_featured_images === "yes") {
												$html .= '<div class="latest_post_two_image">';

												if ($featured_image_size !== 'custom' || $image_width == '' || $image_height == '') {
													$html .= get_the_post_thumbnail(get_the_ID(), $image_size);
												} else {
													$html .= qode_generate_thumbnail(get_post_thumbnail_id(get_the_ID()), null, $image_width, $image_height);
												}
												$html .= '</div>';
											}
											$html .= '<div class="latest_post_text"><div class="latest_post_inner"><div class="latest_post_text_inner">';

											$html .= '<h5 itemprop="name" class="latest_post_title entry_title"><a itemprop="url" href="' . get_permalink() . '">' . get_the_title() . '</a></h5>';

											$html .= '<div class="post_infos">';

											if ($text_length != '0') {
												$excerpt = ($text_length > 0) ? mb_substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt();
												$html .= '<span class="date_hour_holder"><div itemprop="dateCreated" class="post_info_date entry_date updated">' . get_the_time('d M Y') . '</div></span>';
											}

											$html .= '<a class="category-link" itemprop="url" href="' . get_category_link($cat[0]->term_id) . '">' . $cat[0]->cat_name . '</a>';

											$html .= '</div>';

											$html .= '</div>';
											$html .= '</li>';

										endwhile;


										$html .= "</ul></div>";

										echo $html;
										wp_reset_postdata();
										?>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-4">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<?php echo do_shortcode('[year_archive category_name="' . $wp_query->query_vars['category_name'] . '"]'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php if (isset($bridge_qode_options['overlapping_content']) && $bridge_qode_options['overlapping_content'] == 'yes') { ?>
			</div>
		</div>
	<?php } ?>
</div>
<?php get_footer(); ?>
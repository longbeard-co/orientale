<?php

// Register Devotional Images CPT
function devotional_image() {
  $labels = array(
    'name'                  => _x('Devotional Images', 'Post Category General Name', 'orientale'),
    'singular_name'         => _x('Devotional Image', 'Post Category Singular Name', 'orientale'),
    'menu_name'             => __('Devotional Images', 'orientale'),
    'name_admin_bar'        => __('Devotional Image', 'orientale'),
    'archives'              => __('Devotional Image Archives', 'orientale'),
    'attributes'            => __('Devotional Image Attributes', 'orientale'),
    'parent_item_colon'     => __('Parent Devotional Image:', 'orientale'),
    'all_items'             => __('All Devotional Images', 'orientale'),
    'add_new_item'          => __('Add New Devotional Image', 'orientale'),
    'add_new'               => __('Add New', 'orientale'),
    'new_item'              => __('New Devotional Image', 'orientale'),
    'edit_item'             => __('Edit Devotional Image', 'orientale'),
    'update_item'           => __('Update Devotional Image', 'orientale'),
    'view_item'             => __('View Devotional Image', 'orientale'),
    'view_items'            => __('View Devotional Images', 'orientale'),
    'search_items'          => __('Search Devotional Image', 'orientale'),
    'not_found'             => __('Not found', 'orientale'),
    'not_found_in_trash'    => __('Not found in Trash', 'orientale'),
    'featured_image'        => __('Featured Image', 'orientale'),
    'set_featured_image'    => __('Set featured image', 'orientale'),
    'remove_featured_image' => __('Remove featured image', 'orientale'),
    'use_featured_image'    => __('Use as featured image', 'orientale'),
    'insert_into_item'      => __('Insert into Devotional Image', 'orientale'),
    'uploaded_to_this_item' => __('Uploaded to this Devotional Image', 'orientale'),
    'items_list'            => __('Devotional Images list', 'orientale'),
    'items_list_navigation' => __('Devotional Images list navigation', 'orientale'),
    'filter_items_list'     => __('Filter Devotional Images list', 'orientale'),
  );
  $args = array(
    'label'                 => __('Devotional Image', 'orientale'),
    'description'           => __('Individual Devotional Image pages'),
    'labels'                => $labels,
    'supports'              => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
    'taxonomies'            => array('devotional_image_category'), // match the registered post type slug
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'show_in_rest'          => true,
    'menu_position'         => 6,
    'menu_icon'             => 'dashicons-format-image',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type('devotional_image', $args);
}
add_action('init', 'devotional_image', 0);


// Custom taxonomy for Devotional Image

add_action('init', 'devotional_image_category', 0);

function devotional_image_category() {

  $labels = array(
    'name' => _x('Devotional Image Categories', 'taxonomy general name'),
    'singular_name' => _x('Devotional Image Category', 'taxonomy singular name'),
    'search_items' =>  __('Search Devotional Image Categories'),
    'all_items' => __('All Devotional Image Categories'),
    'parent_item' => __('Parent Devotional Image Category'),
    'parent_item_colon' => __('Parent Devotional Image Category:'),
    'edit_item' => __('Edit Devotional Image Category'),
    'update_item' => __('Update Devotional Image Category'),
    'add_new_item' => __('Add New Devotional Image Category'),
    'new_item_name' => __('New Devotional Image Category Name'),
    'menu_name' => __('Devotional Image Categories'),
  );

  register_taxonomy('devotional_image_category', array('devotional_image'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'show_in_rest'  => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'share-to-care'),
  ));
}

<?php

/**
 * Template Name: Donate
 */

// make links relative to the language
$current_language = get_locale() == "it_IT" ? "it" : "en";

get_header();
?>

<section class='lb-row'>
  <div class='lb-row-inner'>
    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <div class="image responsive"><img itemprop="image" src="/en/wp-content/uploads/2016/09/DSC00254b.jpg" alt="&nbsp;"> </div>
      </div>
    </div>
  </div>
</section>

<section class='lb-row hero'>
  <div class='lb-row-inner'>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <h1><?php _e('Donate', 'lb'); ?></h1>
      </div>
    </div>
  </div>
</section>

<section class='lb-row lrg-gutters top bottom your-gift'>
  <div class='lb-row-inner'>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php the_field('text'); ?>
      </div>
    </div>
    <?php $video = get_field('video'); ?>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <div class="video">
          <div class="video__bg">
          </div>
          <a href="https://youtu.be/<?php echo $video['id']; ?>" class="video__wrapper" target="_blank" rel="noopener noreferrer" data-lity>
            <img src="https://img.youtube.com/vi/<?php echo $video['id']; ?>/maxresdefault.jpg" />
            <div class="video__play">
              <svg width="32" height="38" viewBox="0 0 32 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M32 18.9875L0.518749 37.1632L0.518751 0.811791L32 18.9875Z" fill="currentColor" />
              </svg>
            </div>
          </a>
          <h5><?php echo $video['title']; ?></h5>
        </div>
      </div>
    </div>
  </div>
</section>

<section class='lb-row lrg-gutters bottom columns'>
  <div class='lb-row-inner'>
    <div class='lb-col col-4'>
      <div class='lb-col-inner'>
        <img src="/en/wp-content/uploads/2016/09/Fish.png" />
        <h4><?php _e("We're Independent", 'lb'); ?></h4>
        <p><?php _e('Although the Oriental Institute forms the Gregorian Consortium with the Gregorian University and the Biblical Institute, it is academically and financially independent from them.', 'lb'); ?></p>
      </div>
    </div>
    <div class='lb-col col-4'>
      <div class='lb-col-inner'>
        <img src="/en/wp-content/uploads/2016/09/Wallet.png" />
        <h4><?php _e('Affordable Education', 'lb'); ?></h4>
        <p><?php _e('Help us keep the cost of studies low to provide accessibility to students travelling from poor countries.', 'lb'); ?></p>
      </div>
    </div>
    <div class='lb-col col-4'>
      <div class='lb-col-inner'>
        <img src="/en/wp-content/uploads/2016/09/Gift.png" />
        <h4><?php _e('We Need Partners', 'lb'); ?></h4>
        <p><?php _e('Your help is needed for us to carry out our work at the service of the Church. Some church institutions support us, but we look to our benefactors to assist us in meeting our financial needs.', 'lb'); ?></p>
      </div>
    </div>
  </div>
</section>

<section class='lb-row lrg-gutters bottom donate'>
  <div class='lb-row-inner'>
    <h2><?php _e('Give Now', 'lb'); ?></h2>

    <div class="tabs">
      <div class="tabs-nav">
        <div class="tabs-nav-inner">
          <a href="#online" class="tab-nav-item active"><img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 30 21'%3E%3Cpath fill='%23B47A28' d='M25 18.53a2.482 2.482 0 0 0 2.488-2.471L27.5 2.47C27.5 1.11 26.375 0 25 0H5C3.625 0 2.5 1.112 2.5 2.47V16.06c0 1.359 1.125 2.47 2.5 2.47H0C0 19.89 1.125 21 2.5 21h25c1.375 0 2.5-1.112 2.5-2.47h-5ZM5 2.47h20V16.06H5V2.47Zm10 17.295c-.688 0-1.25-.556-1.25-1.236 0-.679.563-1.235 1.25-1.235.688 0 1.25.556 1.25 1.235 0 .68-.563 1.236-1.25 1.236Z'/%3E%3C/svg%3E" /> <?php _e('Online', 'lb'); ?></a>
          <a href="#by-mail" class="tab-nav-item"><img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 25 20'%3E%3Cpath fill='%23B47A28' d='M25 2.5C25 1.125 23.875 0 22.5 0h-20A2.507 2.507 0 0 0 0 2.5v15C0 18.875 1.125 20 2.5 20h20c1.375 0 2.5-1.125 2.5-2.5v-15Zm-2.5 0-10 6.25-10-6.25h20Zm0 15h-20V5l10 6.25L22.5 5v12.5Z'/%3E%3C/svg%3E" /> <?php _e('By Mail', 'lb'); ?></a>
        </div>
      </div>

      <div class="tab-bodies">
        <div class="tab-body" id="online">
          <div class="tab-body-inner">
            <?php
            $gf_id = get_locale() == "it_IT" ? "7" : "11";
            echo do_shortcode(
              '[gravityform id="' .
                $gf_id .
                '" title="false" description="false" ajax="true"]'
            );
            ?>
          </div>
        </div>
        <div class="tab-body" id="by-mail">
          <div class="tab-body-inner">
            <div class="int-us-wrapper">
              <div class="international">
                <h5><?php _e('Italy & International', 'lb'); ?></h5>
                <img src="/en/wp-content/uploads/2016/09/Italy-300x200.png" />
                <p><strong><?php _e('Address', 'lb'); ?></strong></p>
                <p>Piazza di S. Maria Maggiore, 7</p>
                <p>00185 Roma, Italy</p>
                <p><strong><?php _e('Phone', 'lb'); ?></strong></p>
                <p>+39 06 4474170</p>
                <p><strong><?php _e('Email', 'lb'); ?></strong></p>
                <p><a href="mailto:ufficiostampa@orientale.it">ufficiostampa@orientale.it</a></p>
              </div>
              <div class="us">
                <h5><?php _e('United States', 'lb'); ?></h5>
                <img src="/en/wp-content/uploads/2016/09/USA-300x174.png" />
                <p><strong><?php _e('Address', 'lb'); ?></strong></p>
                <p>3700 O Street NW, Wolfington Hall</p>
                <p>Washington DC 20057-0002</p>
                <p><strong><?php _e('Phone', 'lb'); ?></strong></p>
                <p>855-854-8008 (Toll Free)</p>
                <p><strong>Fax</strong></p>
                <p>202-687-7679</p>
              </div>
            </div>
            <p class="donations-text"><?php _e('The Oriental Institute gratefully accepts donations, which are tax deductible in the United States, through The Gregorian University Foundation. Donations to the Pontifical Oriental Institute need to mention the name of the Institute. Your generosity to our work is deeply appreciated.', 'lb'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
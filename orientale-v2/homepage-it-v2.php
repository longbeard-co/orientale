<?php

/**
 * Template Name: Homepage - IT - Version 2
 */

// make links relative to the language
$current_language = get_locale() == 'it_IT' ? 'it' : 'en';

get_header(); ?>
<?php $hero_area = get_field('hero_area'); ?>
<section class='lb-row hero' style="background: url('<?php echo get_the_post_thumbnail_url(); ?>');">
  <div class='lb-row-inner'>

    <div class="latests">
      <h4><?php _e('Latest', 'lb'); ?></h4>
      <div class="latests-inner">

        <?php
        $exclude = get_locale() == 'it_IT' ? 60 : 63;

        $args = array(
          'post_type' => 'post',
          'posts_per_page' => 5,
          'category__not_in' => array($exclude),
          // 'date_query' => array(
          //   'after' => array(
          //     'year' => 2019,
          //     'month' => 6,
          //     'day' => 5
          //   ),
          // ),
        );

        $my_query = new WP_Query($args);
        if ($my_query->have_posts()) {

          while ($my_query->have_posts()) {

            $my_query->the_post(); ?>

            <div class="latest">
              <div class="latest-inner">

                <div class="info">
                  <a href="<?php echo get_the_permalink(); ?>">
                    <h5 class="name"><?php echo get_the_title(); ?></h5>
                  </a>
                  <div class="categories">
                    <?php $categories = get_the_category(get_the_ID());
                    foreach ($categories as $category) { ?>
                      <a class="cat" href="/<?php echo $current_language; ?>/category/<?php echo $category->slug; ?>/"><?php echo $category->name; ?></a>
                    <?php } ?>
                  </div>
                </div>

              </div>
            </div>

        <?php }
        }
        wp_reset_postdata();
        ?>

      </div>
      <a href="<?php echo $hero_area['view_more_button_link']['url']; ?>" class="btn-2"><?php _e('View More', 'lb'); ?></a>
    </div>

    <div class="hero-video">
      <div class="hero-video__bg">
        <a href="//www.youtube.com/watch?v=1gyj1CRmyAQ" class="hero-video__wrapper" target="_blank" rel="noopener noreferrer" data-lity>
          <img src="https://orientale.it/en/wp-content/uploads/2021/03/1gyj1CRmyAQ-SD-1-1-1.jpg" />
          <div class="hero-video__play">
            <svg width="32" height="38" viewBox="0 0 32 38" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M32 18.9875L0.518749 37.1632L0.518751 0.811791L32 18.9875Z" fill="currentColor" />
            </svg>
          </div>
        </a>
      </div>
    </div>


    <img src="<?php echo $hero_area['logo']['url']; ?>" alt="<?php echo $hero_area['logo']['alt']; ?>" class="logo">
  </div>
</section>

<?php $study_at_the_orientale = get_field('study_at_the_orientale'); ?>
<section class='lb-row study top bottom gutters'>
  <div class='lb-row-inner'>
    <h2><?php _e('Study at the Orientale', 'lb'); ?></h2>
    <div class="to-study-wrapper">

      <?php

      while (have_rows('study_at_the_orientale')) : the_row();
        // check if the repeater field has rows of data
        if (have_rows('to_study')) :

          // loop through the rows of data
          while (have_rows('to_study')) : the_row(); ?>

            <div class="to-study">
              <a href="<?php echo get_sub_field('link')['url']; ?>" class="to-study-inner">
                <div class="img">
                  <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['url']; ?>">
                </div>
                <div class="info">
                  <h3 class="title">
                    <?php echo get_sub_field('text'); ?>
                    <?php if (get_sub_field('subtext')) : ?><span class="subtitle"><?php echo get_sub_field('subtext') ?></span><?php endif; ?>
                  </h3>
                </div>
              </a>
            </div>

      <?php
          endwhile;
        endif;
      endwhile;
      ?>
    </div>
  </div>
</section>

<?php $students = get_field('students'); ?>
<section class='lb-row students'>
  <div class='lb-row-inner'>

    <div class='lb-col col-6'>
      <div class='lb-col-inner'>

        <?php $current_student = $students['current_student']; ?>
        <?php $current_student_image = $current_student['image']; ?>
        <div class="current">
          <a href="<?php echo $current_student['link']['url'] ?>" class="curret-inner" style="background-image: url('<?php echo wp_get_attachment_image_src($current_student_image, "full")[0]; ?>');">
            <h2><?php echo $current_student['text']; ?></h2>
          </a>
        </div>

        <?php $prospective_students = $students['prospective_students']; ?>
        <?php $prospective_students_image = $prospective_students['image']; ?>
        <div class="prospective">
          <a href="<?php echo $prospective_students['link']['url'] ?>" class="prospective-inner" style="background-image: url('<?php echo wp_get_attachment_image_src($prospective_students_image, "full")[0]; ?>');">
            <h2><?php echo $prospective_students['text']; ?></h2>
          </a>
        </div>

      </div>
    </div>

    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php $ordo_anni_academici = $students['ordo_anni_academici']; ?>
        <?php $ordo_anni_academici_image = $ordo_anni_academici['image']; ?>
        <?php $ordo_anni_academici_logo_image = $ordo_anni_academici['logo_image']; ?>
        <?php $ordo_anni_academici_student_buttons = $ordo_anni_academici['student_buttons']; ?>
        <div class="ordo">
          <div class="ordo-inner" style="background-image: url('<?php echo wp_get_attachment_image_src($ordo_anni_academici_image['id'], "full")[0]; ?>');">
            <div class="text">
              <div class="logo">
                <img src="<?php echo $ordo_anni_academici_logo_image['url'] ?>" alt="<?php echo $ordo_anni_academici_logo_image['alt'] ?>">
              </div>
              <div class="buttons">
                <?php
                while (have_rows('students')) : the_row();
                  while (have_rows('ordo_anni_academici')) : the_row();
                    if (have_rows('student_buttons')) :
                      while (have_rows('student_buttons')) : the_row(); ?>
                        <a href="<?php echo get_sub_field('link')['url']; ?>" target="_blank">
                          <?php echo get_sub_field('svg_icon'); ?>
                          <span><?php echo get_sub_field('text') ?></span>
                        </a>
                <?php
                      endwhile;
                    endif;
                  endwhile;
                endwhile;
                ?>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</section>

<?php $stable_professors = get_field('stable_professors'); ?>
<section class='lb-row profs top bottom gutters'>
  <div class='lb-row-inner'>

    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <h3><?php _e('Professors', 'lb'); ?></h3>
        <?php $professors = $stable_professors['professors'];
        $finalIDS = array();

        if ($professors) {
          foreach ($professors as $post) {
            setup_postdata($post);
            $postID = $post->ID;
            array_push($finalIDS, $postID);
          }
          wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
        }
        // var_dump($finalIDS);
        ?>
        <div class="stable_professors">
          <div class="stable_professors-inner">

            <?php


            $args = array(
              'post_type' => 'portfolio_page',
              'post__in' => $finalIDS,
              'posts_per_page' => count($finalIDS),
              'orderby' => 'post__in',
            );

            $my_query = new WP_Query($args);
            if ($my_query->have_posts()) {

              while ($my_query->have_posts()) {

                $my_query->the_post(); ?>

                <div class="professor">
                  <a href="<?php echo get_the_permalink(); ?>" class="professor-inner">

                    <div class="img">
                      <div class="img-innner" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

                      </div>
                    </div>

                    <div class="info">
                      <h4 class="name"><?php echo get_the_title(); ?></h4>
                      <p class="role-faculty"><?php if (get_field('role')) : ?><span class="role"><?php echo get_field('role'); ?></span><?php endif; ?><?php if (get_field('faculty')) : ?><span class="faculty"><?php echo get_field('faculty'); ?></span><?php endif; ?></p>
                    </div>

                  </a>
                </div>

            <?php }
            }
            wp_reset_postdata();
            ?>

          </div>
        </div>
        <a href="<?php echo $stable_professors['button_link']['url']; ?>" class="btn top"><?php echo $stable_professors['button_text']; ?></a>
      </div>
    </div>

  </div>
</section>
<?php $news_calendar = get_field('news_calendar'); ?>
<section class='lb-row news-calendar gutters bottom'>
  <div class='lb-row-inner'>

    <div class="tabs">
      <div class="tabs-nav">
        <div class="tabs-nav-inner">
          <a href="#news" class="tab-nav-item active"><?php _e('News', 'lb'); ?></a>
          <a href="#academic-calendar" class="tab-nav-item"><?php _e('Events', 'lb'); ?></a>
          <a href="#deans-blog" class="tab-nav-item"><?php _e("Deans' Blog", 'lb'); ?></a>
        </div>
      </div>

      <div class="tab-bodies">
        <div class="tab-body" id="news">
          <div class="tab-body-inner">
            <div class="news-wrapper">
              <div class="first-col">
                <?php
                $args = array(
                  'post_type' => 'post',
                  'category_name' => 'news',
                  'posts_per_page' => 7,
                  'category__not_in' => array($exclude),
                  // 'date_query' => array(
                  //   'after' => array(
                  //     'year' => 2019,
                  //     'month' => 4
                  //   ),
                  // ),
                );
                $my_query = new WP_Query($args);
                $news_count = 0;
                if ($my_query->have_posts()) {


                  while ($my_query->have_posts()) {
                    $news_count++;
                    $my_query->the_post(); ?>

                    <article class="news">
                      <div class="news-inner">

                        <div class="img">
                          <a class="img-inner" href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

                          </a>
                        </div>

                        <div class="info">
                          <a href="<?php echo get_the_permalink(); ?>">
                            <h4 class="name"><?php echo get_the_title(); ?></h4>
                          </a>
                          <p class="date-cat"><span class="date"><?php echo get_the_date('d M Y'); ?></span><?php $categories = get_the_category(get_the_ID());
                                                                                                            foreach ($categories as $category) { ?>
                              <span class="cat"><a href="/<?php echo $current_language; ?>/category/<?php echo $category->slug; ?>/"><?php echo $category->name; ?></a></span>
                            <?php } ?>
                          </p>
                        </div>

                      </div>
                    </article>
                    <?php if ($news_count == 2) : ?>
              </div>
              <div class="second-col"><?php endif; ?>


          <?php  }
                }
                wp_reset_postdata();
          ?>
              </div>
            </div>

            <a class="btn top" href="<?php echo $news_calendar['news_button_link']['url']; ?>"><?php echo $news_calendar['news_button_text']; ?></a>
          </div>
        </div>
        <div class="tab-body" id="academic-calendar">
          <div class="tab-body-inner">
            <div class="pre">
              <h4><?php _e('Important Events', 'lb'); ?></h4>
              <!--<a href="/en/events"><svg xmlns="http://www.w3.org/2000/svg" width="26.021" height="28.624" viewBox="0 0 26.021 28.624"><path id="Path_3911" data-name="Path 3911" d="M25.419,3.6h-1.3V1h-2.6V3.6H8.505V1H5.9V3.6H4.6A2.61,2.61,0,0,0,2,6.2V27.022a2.61,2.61,0,0,0,2.6,2.6H25.419a2.61,2.61,0,0,0,2.6-2.6V6.2A2.61,2.61,0,0,0,25.419,3.6Zm0,23.419H4.6V10.108H25.419Z" transform="translate(-2 -1)" fill="#b47b28"></path></svg> <h4>View Full Calendar</h4></a>-->
              <a class="btn" href="/it/eventi"><span><?php _e('View Full Calendar', 'lb'); ?></span></a>
            </div>
            <div class="events-list">
              <?php echo do_shortcode("[tribe_events_list limit='4']") ?></div>
            <!-- <div class="lb_events_more">
              <a href="https://orientale.longbeardcode.com/en/events/" rel="bookmark">View More&nbsp;&nbsp;+</a>
            </div> -->
            <a class="btn" href="<?php echo $news_calendar['calendar_button_link']['url']; ?>"><span><?php echo $news_calendar['calendar_button_text']; ?></span></a>
          </div>
        </div>
        <div class="tab-body" id="deans-blog">
          <div class="tab-body-inner">
            <div class="deans-wrapper">
              <div class="first-col" style="border: none;">
                <?php
                $argss = array(
                  'post_type' => 'post',
                  'category_name' => 'deans-blog',
                  'posts_per_page' => 7
                  // 'date_query' => array(
                  //     'after' => array(
                  //         'year' => 2019,
                  //         'month' => 4
                  //     ),
                  // ),
                );
                $deans_count = 0;
                $my_queryy = new WP_Query($argss);
                if ($my_queryy->have_posts()) {

                  while ($my_queryy->have_posts()) {
                    $deans_count++;
                    $my_queryy->the_post(); ?>

                    <article class="deans">
                      <div class="deans-inner">

                        <div class="img">
                          <a class="img-inner" href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

                          </a>
                        </div>

                        <div class="info">
                          <p class="date-cat">
                            <span class="date"><?php echo get_the_date('d M Y'); ?></span>
                            <?php
                            $categories = get_the_category(get_the_ID());
                            foreach ($categories as $category) { ?>
                              <span class="cat">
                                <a href="/<?php echo $current_language; ?>/category/<?php echo $category->slug; ?>/"><?php echo $category->name; ?></a>
                              </span>
                            <?php } ?>
                          </p>
                          <a href="<?php echo get_the_permalink(); ?>">
                            <h4 class="name"><?php echo get_the_title(); ?></h4>
                          </a>
                        </div>
                      </div>
                    </article>
                    <?php if ($deans_count == 2) { ?>
              </div> <!-- first-col -->
              <div class="second-col">
              <?php } ?>

            <?php }
                } else {
            ?>
            <p><?php _e('No posts', 'lb'); ?>.</p>
          <?php
                }
                wp_reset_postdata();
          ?>
              </div> <!-- first-col / second-col -->
            </div> <!-- deans-wrapper -->
            <a class="btn top" href="<?php echo $news_calendar['dean_button_link']['url']; ?>"><?php echo $news_calendar['dean_button_text']; ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $library = get_field('library'); ?>
<section class='lb-row library bottom gutters'>
  <div class='lb-row-inner'>
    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <?php echo $library['text']; ?>
      </div>
    </div>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php echo $library['text']; ?>
      </div>
    </div>

    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php $image = $library['image']; ?>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
      </div>
    </div>
  </div>
</section>

<?php $about_section = get_field('about_section'); ?>
<section class='lb-row  about'>
  <div class='lb-row-inner'>
    <div class="tabs">
      <div class="tabs-inner">
        <?php
        while (have_rows('about_section')) : the_row();
          // check if the repeater field has rows of data
          if (have_rows('about')) :

            // loop through the rows of data
            while (have_rows('about')) : the_row(); ?>

              <a href="#<?php echo sanitize_title(get_sub_field('title')); ?>">
                <h4><?php echo get_sub_field('title'); ?></h4>
              </a>

        <?php endwhile;

          else :

          // no rows found

          endif;
        endwhile;
        ?>

      </div>
    </div>

    <?php
    while (have_rows('about_section')) : the_row();
      // check if the repeater field has rows of data
      if (have_rows('about')) :

        // loop through the rows of data
        while (have_rows('about')) : the_row(); ?>

          <div class='about item lb-col col-<?php echo 12 / count($about_section['about']); ?>' id="<?php echo sanitize_title(get_sub_field('title')); ?>">
            <div class='lb-col-inner'>
              <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
              <div class="info"><?php echo get_sub_field('text'); ?></div>
              <a href="<?php echo get_sub_field('button_link')['url']; ?>" class="btn top"><?php echo get_sub_field('button_text'); ?></a>
            </div>
          </div>

    <?php endwhile;

      else :

      // no rows found

      endif;
    endwhile;
    ?>

  </div>
</section>

<?php $publications = get_field('publications'); ?>
<section class='lb-row publications top bottom '>
  <div class='lb-row-inner'>

    <div class='lb-col featured col-4'>
      <div class='lb-col-inner'>
        <h3><?php _e('Featured Publication', 'lb'); ?></h3>
        <?php $featured_publication_ID = $publications['featured_publication']; ?>
        <article class="featured-publication">
          <a href="<?php echo get_the_permalink($featured_publication_ID); ?>" class="featured-publication-inner">
            <div class="img">
              <div class="img-inner" style="background-image: url('<?php echo get_the_post_thumbnail_url($featured_publication_ID) ?>');"></div>
            </div>
            <div class="info">
              <h4 class="title"><?php echo get_the_title($featured_publication_ID); ?></h4>
              <?php if (get_field('subtitle', $featured_publication_ID)) : ?><p class="subtitle"><?php echo get_field('subtitle', $featured_publication_ID); ?></p><?php endif; ?>
              <?php if (get_field('author', $featured_publication_ID)) : ?><p class="author"><?php echo get_field('author', $featured_publication_ID); ?></p><?php endif; ?>
              <p class="date"><?php echo get_the_date('d M Y', $featured_publication_ID); ?></p>
            </div>
          </a>
        </article>
      </div>
    </div>

    <div class='lb-col feed col-8'>
      <div class='lb-col-inner'>
        <h3><?php echo $publications['publications_title']; ?></h3>
        <div class="pub-note">
          <?php echo $publications['publications_text']; ?>
        </div>
        <div class="publications-inner">

          <?php
          $publicationsIDs = array();

          if ($publications['publications_feed']) {
            $publications_feed = $publications['publications_feed'];
            if ($professors) {
              foreach ($publications_feed as $post) {
                setup_postdata($post);
                $postID = $post->ID;
                array_push($publicationsIDs, $postID);
              }
              wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
            }
          }



          $initalPostsArgs = array(
            'post_type' => 'product',
            'posts_per_page' => 2,
            'post__not_in' => $publicationsIDs,
            'orderby' => 'date',
            'order' => 'DESC',
            'fields' => 'ids',
          );

          $q = get_posts($initalPostsArgs);

          // var_dump($publicationsIDs);

          $merged_ids = array_merge($publicationsIDs, $q);

          $args = array(
            'post_type' => 'product',
            'post__in' => $merged_ids,
            'orderby' => 'post__in',
            'posts_per_page' => 2
          );

          $my_query = new WP_Query($args);
          if ($my_query->have_posts()) {

            while ($my_query->have_posts()) {

              $my_query->the_post(); ?>

              <div class="publication">
                <a href="<?php echo get_the_permalink(); ?>" class="publication-inner">

                  <div class="img">
                    <div class="img-inner" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

                    </div>
                  </div>

                  <div class="info">
                    <h4 class="name"><?php echo get_the_title(); ?></h4>
                    <?php if (get_field('subtitle')) : ?><p class="subtitle"><?php echo get_field('subtitle'); ?></p><?php endif; ?>
                    <?php if (get_field('author')) : ?><p class="author"><?php echo get_field('author'); ?></p><?php endif; ?>
                    <p class="date"><?php echo get_the_date('d M Y'); ?></p>
                  </div>

                </a>
              </div>

          <?php }
          }
          wp_reset_postdata();
          ?>

        </div>
      </div>
    </div>



    <a href="<?php echo $publications['button_link']['url']; ?>" class="btn top"><?php echo $publications['button_text']; ?></a>

  </div>
</section>

<?php $our_conference_venue = get_field('our_conference_venue'); ?>
<section class='lb-row venue top bottom gutters'>
  <div class='lb-row-inner'>

    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php echo $our_conference_venue['text']; ?>
        <a href="<?php echo $our_conference_venue['button_link']['url']; ?>" class="btn top"><?php echo $our_conference_venue['button_text']; ?></a>
      </div>
    </div>

    <div class='lb-col col-6'>
      <div class='lb-col-inner'>

        <?php
        $images = $our_conference_venue['slider'];
        if ($images) : ?>
          <ul class="slider">
            <?php foreach ($images as $image) : ?>
              <div class="image">
                <div class="image-inner" style="background-image: url('<?php echo $image['url'] ?>');">

                </div>
              </div>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>

        <a href="<?php echo $our_conference_venue['button_link']['url']; ?>" class="btn top"><?php echo $our_conference_venue['button_text']; ?></a>
      </div>
    </div>

  </div>
</section>

<?php $our_notable_alumni = get_field('our_notable_alumni'); ?>
<section class='lb-row alumni top bottom gutters'>
  <div class='lb-row-inner'>

    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <?php echo $our_notable_alumni['text']; ?>
        <div class="alumnis">
          <div class="alumnis-inner">

            <?php
            while (have_rows('our_notable_alumni')) : the_row();
              // check if the repeater field has rows of data
              if (have_rows('alumni')) :

                // loop through the rows of data
                while (have_rows('alumni')) : the_row(); ?>

                  <div class="alumni <?php echo sanitize_title(get_sub_field('name')); ?>">
                    <div class="alumni-inner">

                      <div class="img">
                        <div class="img-inner" style="background-image: url('<?php echo get_sub_field('photo')['url']; ?>');">

                        </div>
                      </div>

                      <div class="info">
                        <h4 class="name"><?php echo get_sub_field('name'); ?></h4>
                        <?php if (get_sub_field('role')) : ?><p class="role"><?php echo get_sub_field('role'); ?></p><?php endif; ?>
                        <div class="text">
                          <?php echo get_sub_field('info'); ?>
                        </div>
                        <a href="#learn-more" class="btn-2">Learn More</a>
                      </div>

                    </div>
                  </div>

            <?php endwhile;

              else :

              // no rows found

              endif;

            endwhile;

            ?>



          </div>
        </div>

        <a href="<?php echo $our_notable_alumni['button_link']['url']; ?>" class="btn top"><?php echo $our_notable_alumni['button_text']; ?></a>
      </div>
    </div>

  </div>
</section>

<?php $support_the_future_of_eastern_christianity = get_field('support_the_future_of_eastern_christianity'); ?>
<section class='lb-row support top bottom gutters' style="background-image: url('<?php echo $support_the_future_of_eastern_christianity['background_image']['url']; ?>');">
  <div class='lb-row-inner'>
    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <h3><?php _e('Support the future of Eastern Christianity', 'lb'); ?></h3>
      </div>
    </div>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php echo $support_the_future_of_eastern_christianity['support_the_future_of_eastern_christianity_text']; ?>
        <a href="<?php echo $support_the_future_of_eastern_christianity['button_link']['url']; ?>" class="btn top"><?php echo $support_the_future_of_eastern_christianity['button_text']; ?></a>
      </div>
    </div>
    <div class='lb-col col-6'>
      <div class='lb-col-inner'>
        <?php echo $support_the_future_of_eastern_christianity['side_list']; ?>
        <a href="<?php echo $support_the_future_of_eastern_christianity['button_link']['url']; ?>" class="btn top"><?php echo $support_the_future_of_eastern_christianity['button_text']; ?></a>
      </div>
    </div>
  </div>
</section>

<section id="subscribe" class='lb-row subscribe top bottom'>
  <div class='lb-row-inner'>
    <div class='lb-col col-12'>
      <div class='lb-col-inner'>
        <?php
        $gf_id = get_locale() == 'it_IT' ? '1' : '8';
        echo do_shortcode('[gravityform id="' . $gf_id . '" title="true" description="false" ajax="true"]')
        ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
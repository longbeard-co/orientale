var $ = jQuery.noConflict();
/*********************
ANONYMOUS FUNCTIONS
*********************/
$(document).ready(function () {
  console.log("JMJ");
  accessibility();
  headerSearch();
  tabPressedAction();
  staffSlider(".profs .stable_professors .stable_professors-inner");
  venueSlider(".venue .slider");
  tabs(".news-calendar .tabs-nav .tab-nav-item");
  tabs(".donate .tabs-nav .tab-nav-item");
  aboutTabs(".about .tabs a");
  readMoreBtns();
  alumniSlider(".alumnis-inner");
  notableAlumniSlider("#notable-alumni-slider .vc_column-inner>.wpb_wrapper");
  //   wrapWithATag(".page-id-4269 .latest_post_holder>ul>li"); // looks like dead
  //   wrapWithATag(".page-id-4269 .latest_post_two_holder>ul>li"); // looks like dead
  //   wrapWithATag(".page-id-2335 .latest_post_holder>ul>li");
  //   wrapWithATag(".page-id-2335 .latest_post_two_holder>ul>li");
  //   wrapWithATag(".archive .latest_post_two_holder>ul>li");
  //   wrapWithATag(".archive .latest_post_holder>ul>li");
  //   wrapWithATag(".page-id-5263 .latest_post_two_holder>ul>li");
  //   wrapWithATag(".page-id-5263 .latest_post_holder>ul>li");
  //   wrapWithATag(".page-id-796 .latest_post_two_holder>ul>li");
  //   wrapWithATag(".page-id-796 .latest_post_holder>ul>li");
  wrapInterviews();
  mobileMenuSignIn();
  horizontalScroll(".devotional-images__categories__list ul");
  devotionalImages();

  if ($("body").hasClass("page-id-756")) {
    openTabWithHash(".page-id-756");
  }

  if (
    ($("body").hasClass("page-id-97") &&
      window.location.href.indexOf("/en/donate/#notable-alumni") > 0) ||
    window.location.href.indexOf("/it/donazioni/#ex-alunni-importanti") > 0
  ) {
    openAccordWithHash(".page-id-97");
  }

  readMore(".hidden-interviews", 0, 9999);

  readMore(".single-product .desktop_hide.read_more", 5, 601);
  // coursesRedirect();

  // WooCommerce product page template fix
  publicationsFix();

  heroHeight();

  $(document).bind("gform_post_render", function () {
    lbDonate();
    currencyConvert();
  });
});
/*********************
DECLARED FUNCTIONS
*********************/
function accessibility() {
  // Remove focus from links
  document.body.classList.add("is-mouse");

  // Listen to tab events to enable outlines (accessibility improvement)
  document.body.addEventListener("keyup", function (e) {
    if (e.key === "Tab") {
      document.body.classList.remove("is-mouse");
    }
  });

  // Let the document know when the mouse is being used
  document.body.addEventListener("mousedown", () => {
    document.body.classList.add("is-mouse");
  });
}

function headerSearch() {
  const wrappers = document.querySelectorAll(".header-search");
  if (!wrappers.length) {
    return;
  }

  function init(el) {
    const trigger = el.querySelector(".header-search__trigger");
    const dropdown = el.querySelector(".header-search__dropdown");

    function handleClickOutsideDropdown(e) {
      if (!e.target.closest("#header-search")) {
        dropdown.classList.remove("active");
        document.removeEventListener("click", handleClickOutsideDropdown);
      }
    }

    trigger.addEventListener("click", function () {
      dropdown.classList.toggle("active");
      if (dropdown.classList.contains("active")) {
        dropdown.querySelector("input").focus();
        document.addEventListener("click", handleClickOutsideDropdown);
      } else {
        document.removeEventListener("click", handleClickOutsideDropdown);
      }
    });
  }

  for (let i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}

function openTabWithHash(pageId) {
  var currentHash;
  if (window.location.hash) {
    currentHash = window.location.hash;
    setTimeout(function () {
      window.scrollTo(0, 0);
    }, 1);
  }
  var tabs = $(pageId + " .tabs-nav a");
  tabs.each(function () {
    if ($(this).attr("href") == currentHash) {
      var targetEle = $(this);

      function clickFunc() {
        targetEle.closest("li").click();
      }

      function scrollToTarget() {
        // console.log($(targetEle).offset().top);
        window.scrollTo(0, $(targetEle).offset().top - 100);
      }
      setTimeout(clickFunc, 1500);
      setTimeout(scrollToTarget, 1000);
    }
  });
}

function openAccordWithHash(pageId) {
  setTimeout(function () {
    var currentHash;
    if (window.location.hash) {
      currentHash = window.location.hash.slice(1);
    }
    var rows = $(pageId + " .vc_row");
    rows.each(function () {
      if ($(this).attr("id") == currentHash) {
        $(this).find(".more_facts_button").click();
      }
    });
  }, 1000);
}

function wrapWithATag(ele) {
  $(ele).each(function () {
    var eleLink = $(this).find(".latest_post_title > a").attr("href");
    // console.log(eleLink);
    $(this).wrapInner('<a href="' + eleLink + '"></a>');
  });
}

function readMoreBtns() {
  $(".alumnis").on("click", ".btn-2", function (e) {
    e.preventDefault();
    $(this).prev().slideToggle();
    $(this).toggleClass("active");
  });
}

function tabs(tab_nav_items) {
  $(tab_nav_items).click(function (e) {
    e.preventDefault();
    var target = $(this).attr("href");

    function RemoveActiveClass(ele) {
      $(ele).each(function () {
        $(this).removeClass("active");
      });
    }
    RemoveActiveClass($(".tab-nav-item"));
    RemoveActiveClass($(".tab-bodies .tab-body"));
    $(this).addClass("active");
    $(".tab-bodies .tab-body" + target).addClass("active");
  });
  $(tab_nav_items).first().click();
  $(tab_nav_items).first().click();
}

function aboutTabs(tab_nav_item) {
  $(tab_nav_item).click(function (e) {
    e.preventDefault();
    var target = $(this).attr("href");

    function RemoveActiveClass(ele) {
      $(ele).each(function () {
        $(this).removeClass("active");
      });
    }
    RemoveActiveClass($(".about .tabs a"));
    RemoveActiveClass($(".about.lb-col"));
    $(".about.lb-col" + target).addClass("active");
    $(this).addClass("active");
  });
  $(tab_nav_item).first().click();
}

function staffSlider(slider) {
  $(slider).slick({
    infinite: true,
    speed: 400,
    autoplay: false,
    dots: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    adaptiveHeight: true,
    prevArrow:
      '<div class="slick-arrow slick-prev"><img src="/en/wp-content/uploads/2019/08/left-chevron.svg" alt="Left Arrow" /></div>', //replace arrows
    nextArrow:
      '<div class="slick-arrow slick-next"><img src="/en/wp-content/uploads/2019/08/right-chev.svg" alt="Right Arrow" /></div>', //replace arrows
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  });
}

function venueSlider(slider) {
  $(slider).slick({
    infinite: true,
    speed: 400,
    autoplay: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow:
      '<div class="slick-arrow slick-prev"><img src="/en/wp-content/uploads/2019/08/left-chevron.svg" alt="Left Arrow" /></div>', //replace arrows
    nextArrow:
      '<div class="slick-arrow slick-next"><img src="/en/wp-content/uploads/2019/08/right-chev.svg" alt="Right Arrow" /></div>', //replace arrows
  });
}

function alumniSlider(slider) {
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      /* Initializes a slick carousel only on mobile screens */
      // slick on mobile
      if ($(window).width() > 1025) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: true,
            speed: 400,
            autoplay: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow:
              '<div class="slick-arrow slick-prev"><img src="/en/wp-content/uploads/2019/08/left-chevron.svg" alt="Left Arrow" /></div>', //replace arrows
            nextArrow:
              '<div class="slick-arrow slick-next"><img src="/en/wp-content/uploads/2019/08/right-chev.svg" alt="Right Arrow" /></div>', //replace arrows
            responsive: [
              {
                breakpoint: 601,
                settings: {
                  slidesToShow: 1,
                },
              },
            ],
          });
        }
      }
    });
  });
}

function notableAlumniSlider(slider) {
  // console.log(slider);
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      if ($(window).width() >= 1025 || $(window).width() < 600) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: false,
            speed: 400,
            autoplay: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            rows: 0,
            prevArrow:
              '<div class="slick-arrow slick-prev"><img src="/en/wp-content/uploads/2019/08/left-chevron.svg" alt="Left Arrow" /></div>', //replace arrows
            nextArrow:
              '<div class="slick-arrow slick-next"><img src="/en/wp-content/uploads/2019/08/right-chev.svg" alt="Right Arrow" /></div>', //replace arrows
            responsive: [
              {
                breakpoint: 601,
                settings: "unslick",
              },
            ],
          });
        }
      }
    });
  });
}

function readMore(jObj, lineNum, viewPort) {
  if (isNaN(lineNum)) {
    lineNum = 4;
  }

  var go = new ReadMore(jObj, lineNum, viewPort);
}
//class
function ReadMore(_jObj, lineNum, viewPort) {
  if ($(window).width() <= viewPort) {
    $(_jObj).each(function () {
      if (
        $("html").attr("lang") == "en-US" &&
        $("body").hasClass("page-id-798")
      ) {
        var READ_MORE_LABEL = "View More";
      } else if (
        $("html").attr("lang") == "it-IT" &&
        $("body").hasClass("page-id-798")
      ) {
        var READ_MORE_LABEL = "Visualizza altro";
      } else if ($("html").attr("lang") == "it-IT") {
        var READ_MORE_LABEL = "Vedere di Più";
      } else if ($("html").attr("lang") == "es-ES") {
        var READ_MORE_LABEL = "Vea Más";
      } else {
        var READ_MORE_LABEL = "Read More";
      }
      if (
        $("html").attr("lang") == "en-US" &&
        $("body").hasClass("page-id-798")
      ) {
        var HIDE_LABEL = "View Less";
      } else if (
        $("html").attr("lang") == "it-IT" &&
        $("body").hasClass("page-id-798")
      ) {
        var HIDE_LABEL = "Visualizza di meno";
      } else if ($("html").attr("lang") == "it-IT") {
        var HIDE_LABEL = "Vedere di Meno";
      } else if ($("html").attr("lang") == "es-ES") {
        var HIDE_LABEL = "Ver Menos";
      } else {
        var HIDE_LABEL = "Read Less";
      }
      var jObj = $(this);
      var textMinHeight =
        "" +
        parseInt(jObj.children(".hidden-content").css("line-height"), 10) *
          lineNum +
        "px";
      var textMaxHeight = "" + jObj.children(".hidden-content").css("height");
      jObj.find(".hidden-content").css("height", "" + textMaxHeight);
      jObj.find(".hidden-content").css("transition", "height 0.5s");
      jObj.find(".hidden-content").css("height", "" + textMinHeight);
      jObj.append(
        '<button class="read-more btn">' + READ_MORE_LABEL + "</button>"
      );
      jObj.find(".read-more").click(function () {
        if (jObj.find(".hidden-content").css("height") === textMinHeight) {
          jObj.find(".hidden-content").css("height", "" + textMaxHeight);
          jObj.find(".read-more").html(HIDE_LABEL).addClass("active");
        } else {
          jObj.find(".hidden-content").css("height", "" + textMinHeight);
          jObj.find(".read-more").html(READ_MORE_LABEL).removeClass("active");
        }
      });
    });
  }

  function resizing() {
    if ($(window).width() <= viewPort) {
      $(_jObj).each(function () {
        var READ_MORE_LABEL = "Read More";
        var HIDE_LABEL = "Read Less";
        var jObj = $(this);
        var textMinHeight =
          "" +
          parseInt(jObj.children(".hidden-content").css("line-height"), 10) *
            lineNum +
          "px";
        var textMaxHeight =
          "" + jObj.children(".hidden-content")[0].scrollHeight;
        jObj.find(".hidden-content").css("height", "" + textMaxHeight);
        jObj.find(".hidden-content").css("transition", "height 0.5s");
        jObj.find(".hidden-content").css("height", "" + textMinHeight);
        if (!jObj.find(".read-more").length > 0) {
          jObj.append(
            '<button class="read-more btn">' + READ_MORE_LABEL + "</button>"
          );
        }
        jObj.find(".read-more").click(function () {
          if (jObj.find(".hidden-content").css("height") === textMinHeight) {
            jObj.find(".hidden-content").css("height", "" + textMaxHeight);
            jObj.find(".read-more").html(HIDE_LABEL).addClass("active");
          } else {
            jObj.find(".hidden-content").css("height", "" + textMinHeight);
            jObj.find(".read-more").html(READ_MORE_LABEL).removeClass("active");
          }
        });
      });
    } else {
      $(_jObj).each(function () {
        var jObj = $(this);
        jObj.find(".hidden-content").css("height", "");
        jObj.find("button").remove();
      });
    }
  }
  $(window).resize(resizing);
}

cardSlider(".single-product .related.products .products");

function cardSlider(slider) {
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      /* Initializes a slick carousel only on mobile screens */
      // slick on mobile
      if ($(window).width() > 601) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: false,
            speed: 400,
            autoplay: false,
            dots: false,
            // variableWidth: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow:
              '<div class="slick-arrow slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="14.152" height="25.475" viewBox="0 0 14.152 25.475"><path id="arrow-l" data-name="arrow-l" d="M3351,1484l12.03,12.03L3351,1508.06" transform="translate(3364.445 1508.767) rotate(180)" fill="none" stroke="#fff" stroke-width="2"/></svg></div>', //replace arrows
            nextArrow:
              '<div class="slick-arrow slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="14.152" height="25.475" viewBox="0 0 14.152 25.475"><path id="arrow-r" data-name="arrow-r" d="M3351,1484l12.03,12.03L3351,1508.06" transform="translate(-3350.293 -1483.293)" fill="none" stroke="#fff" stroke-width="2"/></svg></div>', //replace arrows
          });
        }
      }
    });
  });
}

function coursesRedirect() {
  $(
    ".page-id-79 .tabs-nav li:nth-child(4), .page-id-77 .tabs-nav li:last-child"
  ).click(function (event) {
    event.preventDefault();
    if ($("html").attr("lang") == "it-IT") {
      window.open("https://orientale.it/it/calendario-accademico/", "_self");
    } else {
      window.open("https://orientale.it/en/academic-calendar/", "_self");
    }
  });
}

function publicationsFix() {
  $("div.woocommerce-product-gallery, div.summary").wrapAll(
    '<div class="publications-top-section-wrapper"></div>'
  );
}

function mobileMenuSignIn() {
  var lang = jQuery("html").attr("lang");
  if (lang == "it-IT") {
    jQuery(".mobile_menu > ul").append(
      '<div class="social-sign-in-wrapper"><div class="social"><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.facebook.com/PontificioIstitutoOrientale/" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-facebook qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://twitter.com/theorientale" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-twitter qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.youtube.com/user/PIOeventi" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-youtube-play qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.linkedin.com/company/pontifical-oriental-institute" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-linkedin qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.instagram.com/theorientale/" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-instagram qode_icon_element" style=""></i></a></span></div><div class="sign-in-wrapper"><a class="sign-in" href="https://mail.google.com/a/orientale.it" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16"><path id="Path_3999" data-name="Path 3999" d="M20,4H4A2,2,0,0,0,2.01,6L2,18a2.006,2.006,0,0,0,2,2H20a2.006,2.006,0,0,0,2-2V6A2.006,2.006,0,0,0,20,4Zm0,4-8,5L4,8V6l8,5,8-5Z" transform="translate(-2 -4)" fill="#b47b28"/></svg>Accedi</a></div></div>'
    );
  } else {
    jQuery(".mobile_menu > ul").append(
      '<div class="social-sign-in-wrapper"><div class="social"><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.facebook.com/PontificioIstitutoOrientale/" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-facebook qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://twitter.com/theorientale" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-twitter qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.youtube.com/user/PIOeventi" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-youtube-play qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.linkedin.com/company/pontifical-oriental-institute" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-linkedin qode_icon_element" style=""></i></a></span><span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" "><a itemprop="url" href="https://www.instagram.com/theorientale/" target="_blank" rel="noopener noreferrer"><i class="qode_icon_font_awesome fa fa-instagram qode_icon_element" style=""></i></a></span></div><div class="sign-in-wrapper"><a class="sign-in" href="https://mail.google.com/a/orientale.it" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16"><path id="Path_3999" data-name="Path 3999" d="M20,4H4A2,2,0,0,0,2.01,6L2,18a2.006,2.006,0,0,0,2,2H20a2.006,2.006,0,0,0,2-2V6A2.006,2.006,0,0,0,20,4Zm0,4-8,5L4,8V6l8,5,8-5Z" transform="translate(-2 -4)" fill="#b47b28"/></svg>Sign In</a></div></div>'
    );
  }
}

function wrapInterviews() {
  var lisEN = $(
    ".tabs-container > div#tab-69ad9e6d-105b-4 > .q_elements_holder"
  );
  lisEN
    .slice(4)
    .wrapAll(
      "<div class='hidden-interviews'><div class='hidden-content'></div></div>"
    );
  var lisIT = $(
    ".tabs-container > div#tab-1480690926678-1-5 > .q_elements_holder"
  );
  lisIT
    .slice(4)
    .wrapAll(
      "<div class='hidden-interviews'><div class='hidden-content'></div></div>"
    );
  var lisIT_it = $(
    ".tabs-container > div#tab-1480690536287-1-6 > .q_elements_holder"
  );
  lisIT_it
    .slice(4)
    .wrapAll(
      "<div class='hidden-interviews'><div class='hidden-content'></div></div>"
    );
  var lisUK = $(
    ".tabs-container > div#tab-1480690928343-2-4 > .q_elements_holder"
  );
  lisUK
    .slice(4)
    .wrapAll(
      "<div class='hidden-interviews'><div class='hidden-content'></div></div>"
    );
  var lisUK_it = $(
    ".tabs-container > div#tab-1480690537740-2-8 > .q_elements_holder"
  );
  lisUK_it
    .slice(4)
    .wrapAll(
      "<div class='hidden-interviews'><div class='hidden-content'></div></div>"
    );
}

// 1. Create <script> tag on head

var tag = document.createElement("script");
tag.src = "https://www.youtube.com/iframe_api";
document.head.insertAdjacentElement("beforebegin", tag);

// 2. This function creates an <iframe> (and YouTube player) after the API code downloads.
let player;

function onYouTubeIframeAPIReady() {
  const iframes = document.querySelectorAll("[data-youtube-id]");

  if (iframes.length) {
    for (let i = 0; i < iframes.length; i++) {
      const youtubeId = iframes[i].getAttribute("data-youtube-id");

      player = new YT.Player(iframes[i], {
        height: "315",
        width: "560",
        videoId: youtubeId,
        events: {
          onReady: onPlayerReady,
        },
      });
    }
  }
}

// 3. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  const target = event.target.h;
  //   console.log(event.target.h);
  const wrapper = target.closest(".video-wrapper");

  if (wrapper) {
    const thumbs = wrapper.querySelector(".video-thumbs");
    const iframeWrapper = wrapper.querySelector(".iframe-wrapper");

    // 1. Listen to thumbnail click
    thumbs.addEventListener("click", function () {
      thumbs.style.display = "none";
      iframeWrapper.classList.add("active");

      // 2. Play Video on click
      event.target.playVideo();
    });
  }
}

function heroHeight() {
  const hero = document.querySelector(".home .hero");
  if (!hero) {
    return;
  }

  function setMinHeight() {
    const heightReference = hero.querySelector(".latests-inner");
    const minHeight = heightReference.clientHeight + 90 + 50 + 100;
    if (minHeight) {
      hero.style.minHeight = minHeight + "px";
    }
  }

  setMinHeight();
  window.addEventListener("resize", setMinHeight);
}

// listen to when tab key is press on window
function tabPressedAction() {
  $(window).on("keydown", function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode == 9) {
      $("#CybotCookiebotDialog").addClass("tab-pressed");
    }
  });
}

function lbDonate() {
  const amountField = document.querySelectorAll(".field--select-amount");

  if (!amountField.length) {
    return;
  }

  for (let i = 0; i < amountField.length; i++) {
    const other = amountField[i].querySelector('[value="gf_other_choice"]');
    const radios = amountField[i].querySelectorAll('input[type="radio"]');

    if (other) {
      const gchoice = other.closest(".gchoice");
      const radio = gchoice.querySelector('input[type="radio"]');
      const text = gchoice.querySelector('input[type="text"]');
      // const label = gchoice.querySelector("label");

      // const fieldId = radio.id;
      // radio.insertAdjacentHTML(
      //   "beforebegin",
      //   `<label for="${fieldId}" + id="${fieldId.replace(
      //     "choice",
      //     "label"
      //   )}"></label>`
      // );
      // label.innerHTML = `<span>${label.innerText}</span>`;

      // const label = gchoice.querySelector("label");

      // label.addEventListener("click", () => text.click());

      // radio.addEventListener("change", function (e) {
      //   if (e.currentTarget.checked) {
      //     label.classList.add("checked");
      //   } else {
      //     label.classList.remove("checked");
      //   }
      // });

      if (radio.checked && text.value && !Number.isNaN(text.value)) {
        radio.value = text.value;
        text.setAttribute("data-value", text.value);
        text.value = formatCurrency(text.value);
      } else {
        // GF automatically disables this
        text.disabled = false;

        // Add placeholder
        text.placeholder = text.getAttribute("value");
        text.value = "";
      }

      text.addEventListener("focus", function (e) {
        const value = e.currentTarget.value;
        const dataValue = e.currentTarget.getAttribute("data-value");

        if (value && dataValue) {
          e.currentTarget.value = dataValue;
        } else {
          e.currentTarget.value = "";
        }
        radio.checked = true;
        radio.click();

        e.currentTarget.classList.remove("placeholder");
      });

      text.addEventListener("blur", function (e) {
        const value = e.currentTarget.value;

        if (value && !isNaN(value)) {
          e.currentTarget.setAttribute("data-value", value);
          const formattedValue = formatCurrency(value);
          e.currentTarget.value = formattedValue;
          radio.checked = true;
          radio.click();
          radio.blur();
        } else {
          e.currentTarget.setAttribute("data-value", "");
          const placeholder = e.currentTarget.getAttribute("value");
          e.currentTarget.value = placeholder;
          radio.click();
          radio.blur();
          radio.checked = false;
        }

        if (e.currentTarget.value !== e.currentTarget.getAttribute("value")) {
          text.classList.remove("placeholder");
        } else {
          text.classList.add("placeholder");
        }
      });

      text.addEventListener("input", function (e) {
        radio.value = !isNaN(e.currentTarget.value) ? e.currentTarget.value : 0;
      });

      if (text.value === text.getAttribute("value")) {
        text.classList.add("placeholder");
      }

      for (let j = 0; j < radios.length; j++) {
        radios[j].addEventListener("change", function (e) {
          if (e.currentTarget.checked && e.currentTarget !== radio) {
            radio.checked = false;
            // label.classList.remove("checked");
          }
          text.disabled = false;
        });
      }
    }
  }

  // Fix input placeholder bug with GF
  gform.addAction(
    "gform_post_conditional_logic_field_action",
    function (formId, action, targetId, defaultValues, isInit) {
      if (targetId === "#field_3_5" || targetId === "#field_3_39") {
        const inputId = targetId.replace("field", "input") + "_other";
        const input = document.querySelector(inputId);
        if (input && input.value.includes("choice_")) {
          // const radio = input
          //   .closest(".gchoice")
          //   .querySelector('input[type="radio"]');
          // input.value = !isNaN(radio.value) ? formatCurrency(radio.value) : "";
          input.value = "";
        }
      }
    }
  );
}

function currencyConvert() {
  const currencySymbols = {
    EUR: "€",
    GBP: "£",
    USD: "$",
  };

  function updateCurrencyDisplay(currency) {
    const symbol = currencySymbols[currency] || "$";
    const gfID = document.documentElement.lang === "it-IT" ? "7" : "11";

    const list = document.querySelectorAll("#input_" + gfID + "_11 li");
    for (let i = 0; i < list.length; i++) {
      const listItem = list[i];
      const listLabel = listItem.querySelector("label");
      if (listLabel) {
        listLabel.innerText = listLabel.innerText.replace(
          /[^\d\.\,\s]+/,
          symbol
        );
      }
    }
    // Other input
    const otherInput = document.querySelector("#input_" + gfID + "_11_other");
    otherInput.value =
      otherInput.value && !Number.isNaN(otherInput.value)
        ? formatCurrency(otherInput.dataset.value) || otherInput.value
        : otherInput.value;
  }

  const inputs = document.querySelectorAll("input[name='input_10']");
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("change", function (e) {
      if (e.currentTarget.checked) {
        const currency = e.currentTarget.value || "USD";
        updateCurrencyDisplay(currency);
      }
    });
  }
}

function formatCurrency(number) {
  if (number && !isNaN(parseInt(number))) {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency:
        document.querySelector(
          '.field--select-currency input[type="radio"]:checked'
        )?.value || "USD",
    }).format(number);
  }
}

function horizontalScroll(element, startReverse) {
  var innerWidth = 0;

  $(element)
    .each(function () {
      if (!$(this).closest(".overflow-wrapper").length) {
        $(this).wrap('<div class="overflow-wrapper"></div>');
      }
      if (!$(this).hasClass("overflow-ready")) {
        $(this).addClass("overflow-ready--x");
      }
      scrollOverflow($(this));
    })
    .scroll(function () {
      scrollOverflow($(this));
    });

  $(window).on("resize", function () {
    setTimeout(function () {
      $(element).each(function () {
        scrollOverflow($(this));
      });
    }, 500);
  });

  function scrollOverflow(el) {
    var wrap = el.closest(".overflow-wrapper");
    if (el[0].scrollWidth > el[0].clientWidth) {
      // your element have overflow
      wrap.addClass("overflow--x");

      if (el.scrollLeft() > 0) {
        wrap.addClass("overflow--left");
      } else {
        wrap.removeClass("overflow--left");
      }

      if (el.scrollLeft() < el.get(0).scrollWidth - el.outerWidth() - 10) {
        wrap.addClass("overflow--right");
      } else {
        wrap.removeClass("overflow--right");
      }
    } else {
      // your element doesn't have overflow
      wrap.removeClass("overflow--x");
    }
  }
}

function devotionalImages() {
  const wrapper = document.getElementById("devotional-images");
  if (!wrapper) return;

  window._devotionalImagesData = [];

  const postsWrapper = document.querySelector(".posts-wrapper");

  // Get Document Language
  const lang = getDocumentLanguage();

  // define per page
  const perPage = 6;

  // Translate Global vars
  let downloadStr, copyStr;
  switch (lang) {
    case "it":
      downloadStr = "Immagine";
      copyStr = "Testo";
      viewMoreStr = "Visualizzare Più";
      break;
    default:
      downloadStr = "Download Image";
      copyStr = "Text";
      viewMoreStr = "View More";
  }

  // Set default state
  history.pushState(
    {
      search: "",
      taxonomy: "",
      pageNo: 1,
    },
    ""
  );

  // Build placeholders HTML
  let placeholders = "";
  for (let i = 0; i < perPage; i++) {
    placeholders += '<article class="devotional devotional--placeholder">';
    placeholders +=
      '<div class="devotional__img"></div><div class="devotional__info"><h3 class="devotional__title"></h3><h4 class="devotional__date"></h4><div class="devotional__buttons"></div></div>';
    placeholders += "</article>";
  }

  // Categories

  const categoryButtons = document.querySelectorAll(
    "#devotional-images-categories button"
  );
  const categoryListItems = document.querySelectorAll(
    "#devotional-images-categories li"
  );

  if (categoryButtons) {
    for (let i = 0; i < categoryButtons.length; i++) {
      categoryButtons[i].addEventListener("click", handleCategoryButtonClick);
    }
  }

  const categorySearchForm = document.querySelector("#category-search-form");
  const categorySearchInput = document.querySelector("#category-search-input");

  categorySearchForm.addEventListener("submit", handleCategorySearchFormSubmit);
  categorySearchInput.addEventListener("keyup", handleCategorySearchInputKeyup);

  // Search Form
  const searchForms = document.querySelectorAll("form.posts-search");
  for (let i = 0; i < searchForms.length; i++) {
    searchForms[i].addEventListener("submit", handleFormSubmit);
  }

  loadPosts({
    taxonomy: "",
    search: "",
    pageNo: 1,
  });

  // Prefetch
  loadPosts(
    {
      taxonomy: "",
      search: "",
      pageNo: 2,
    },
    true
  );

  handleViewMoreClick();
  handleSearchInput();

  function createPaginationArray(currentPage, totalPages) {
    const delta = 1;
    const pagination = [];

    // Push items from "current - 1" (if available) to current + 1 (if available)
    for (
      let i = Math.max(2, currentPage - delta);
      i <= Math.min(totalPages - 1, currentPage + delta);
      i++
    ) {
      // if current = 1, total = 7, pagination[] => [2]
      // if current = 5, total = 7, pagination[] => [4, 5, 6];
      // current = 7, total = 7, pagination[] => [6];
      pagination.push(i);
    }

    // if 3 or more pages exist before current page
    //  items from 2 to current - 2 will be "..."
    if (currentPage - delta > 2) {
      // add "..." to the beginning
      pagination.unshift("...");
    }

    // if 3 or more exists after current page
    // items from current + 2 to lastPage(totalPage) - 1 will be "..."
    if (currentPage + delta < totalPages - 1) {
      // add "..." to the end
      pagination.push("...");
    }

    // Always add 1 (first page) to the beginning
    pagination.unshift(1);
    // Always add totalPage (last page) to the end
    pagination.push(totalPages);

    return pagination;
  }

  function buildPagination(totalPages) {
    const pageNo = history.state?.pageNo ? parseInt(history.state.pageNo) : 1;

    const isFirstPage = pageNo === 1;
    const isLastPage = pageNo === totalPages;
    const isPrevDisabled = isFirstPage ? "disabled" : "";
    const isNextDisabled = isLastPage ? "disabled" : "";

    let html = "";

    if (totalPages > 1) {
      const paginationArray = createPaginationArray(pageNo, totalPages);

      if (paginationArray.length) {
        html += "<ul>";

        html += "<li>";
        html +=
          '<button type="button" class="pagination-page pagination-arrow" data-page="' +
          (pageNo - 1) +
          '" aria-label="Previous Page" ' +
          isPrevDisabled +
          ">" +
          '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 9 14" width="9" height="14"><path fill="#660B21" d="M8.32568 12.516L3.12436 7L8.32568 1.484L6.92634 3.60753e-07L0.325683 7L6.92634 14L8.32568 12.516Z"/></svg>' +
          "</button>";
        html += "</li>";

        for (let i = 0; i < paginationArray.length; i++) {
          const number = paginationArray[i];
          const current = number === pageNo ? " current" : "";
          html += "<li>";
          if (number === "...") {
            html += '<span class="pagination-dots">' + number + "</span>";
          } else {
            html +=
              '<button type="button" class="pagination-page' +
              current +
              '" data-page="' +
              number +
              '">' +
              number +
              "</button>";
          }
          html += "</li>";
        }

        html += "<li>";
        html +=
          '<button type="button" class="pagination-page pagination-arrow" data-page="' +
          (pageNo + 1) +
          '" aria-label="Next Page" ' +
          isNextDisabled +
          ">" +
          '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 8 14" width="8" height="14"><path fill="#660B21" d="M0 1.484L5.20132 7L0 12.516L1.39934 14L8 7L1.39934 0L0 1.484Z"/></svg>' +
          "</button>";
        html += "</li>";

        html += "</ul>";
      }
    }

    const paginationContainers = document.querySelectorAll(".posts-pagination");
    for (let j = 0; j < paginationContainers.length; j++) {
      removeChildren(paginationContainers[j]);
      paginationContainers[j].insertAdjacentHTML("beforeend", html);
    }

    // Add event listener
    const buttons = document.querySelectorAll(".pagination-page");
    for (let k = 0; k < buttons.length; k++) {
      buttons[k].addEventListener("click", function (e) {
        const page = e.currentTarget.getAttribute("data-page");
        if (page) {
          const taxonomy =
            history.state && history.state.taxonomy
              ? history.state.taxonomy
              : "";
          const search =
            history.state && history.state.search ? history.state.search : "";

          loadPosts({
            taxonomy,
            search,
            pageNo: parseInt(page),
          });

          // Prefetch
          loadPosts(
            {
              taxonomy,
              search,
              pageNo: parseInt(page) + 1,
            },
            true
          );
        }
      });
    }
  }

  function populatePosts(results) {
    const { data, wpTotalPages } = results;

    let html = "";

    if (data && data.length) {
      Object.keys(data).map(function (key) {
        const id = data[key].id;
        const title =
          lang !== "en" && data[key].acf.title && data[key].acf.title[lang]
            ? data[key].acf.title[lang]
            : data[key].title.rendered;
        const imgMedium =
          lang !== "en" &&
          data[key].acf.featured_image &&
          data[key].acf.featured_image[lang]
            ? data[key].acf.featured_image[lang]
            : data[key]._embedded["wp:featuredmedia"]
            ? data[key]._embedded["wp:featuredmedia"][0].media_details.sizes[
                "medium_large"
              ]?.source_url
            : "";
        const imgFull =
          lang !== "en" &&
          data[key].acf.featured_image &&
          data[key].acf.featured_image[lang]
            ? data[key].acf.featured_image[lang]
            : data[key]._embedded["wp:featuredmedia"]
            ? data[key]._embedded["wp:featuredmedia"][0].media_details.sizes[
                "full"
              ]?.source_url
            : "";
        const date = formatDate(data[key].date);
        const download =
          data[key].acf && data[key].acf.download_link
            ? data[key].acf.download_link
            : "";

        // let article = document.createElement("article");
        // article.setAttribute("class", "devotional");
        html += '<article class="devotional">';
        html += '<a class="devotional__link" href="' + imgFull + '" data-lity>';
        html += '<div class="devotional__img">';
        html += imgMedium
          ? '<img src="' + imgMedium + '" alt="' + title + '" />'
          : "";
        html += "</div>";
        html += '<h3 class="devotional__title">' + title + "</h3>";
        html += "</a>";
        html += '<div class="devotional__info">';
        html += '<h4 class="devotional__date">' + date + "</h4>";
        html += '<div class="devotional__buttons">';
        html +=
          '<a class="btn" href="#devotional-modal" data-id="' +
          id +
          '" data-lity><svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.25 13.7143H0V16H11.25V13.7143ZM18 4.57143H0V6.85714H18V4.57143ZM0 11.4286H18V9.14286H0V11.4286ZM0 0V2.28571H18V0H0Z" fill="#B47B28"/></svg><span>' +
          copyStr +
          "</span></a>";
        html += download
          ? '<a class="btn" href="#devotional-downloads" data-id="' +
            id +
            '" data-lity><svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.1111 8V14.2222H1.88889V8H0V14.2222C0 15.2 0.85 16 1.88889 16H15.1111C16.15 16 17 15.2 17 14.2222V8H15.1111ZM9.44444 8.59556L11.8906 6.30222L13.2222 7.55556L8.5 12L3.77778 7.55556L5.10944 6.30222L7.55556 8.59556V0H9.44444V8.59556Z" fill="#B47B28"/></svg><span>' +
            downloadStr +
            "</span></a>"
          : "";
        html += "</div>"; // devotional__buttons
        html += "</div>"; // devotional__info
        html += "</article>";
      });
    } else {
      html +=
        '<h4 class="no-content">No content is found. Please adjust your search.</h4>';

      // Try switching tabs
      // const tabs = document.querySelectorAll(".tab-item[data-taxonomy]");
      // [...tabs].some(function (tab) {
      //   const tabID = tab.getAttribute("data-taxonomy");
      //   const tabBody = document.getElementById(
      //     "devotional_image_category-" + tabID + "-search"
      //   );
      //   if (!tabBody.querySelector(".no-content")) {
      //     // switch to this tab
      //     tab.click();
      //     return true;
      //   }
      // });
    }

    // delete placeholders
    // const placeholders = postsWrapper.getElementsByClassName(
    //   "devotional--placeholder"
    // );
    // while (placeholders.length > 0) {
    //   placeholders[0].parentNode.removeChild(placeholders[0]);
    // }
    removeChildren(postsWrapper);
    postsWrapper.insertAdjacentHTML("beforeend", html);

    buildPagination(wpTotalPages);
  }

  function handleViewMoreClick(button) {
    const buttons = button
      ? [button]
      : document.getElementsByClassName("devotional-view-more__btn");
    [].forEach.call(buttons, function (button) {
      button.addEventListener("click", function (e) {
        const taxonomy = e.target.getAttribute("data-taxonomy");
        const totalPages = e.target.getAttribute("data-total-pages");
        let paged = e.target.getAttribute("data-page");
        paged++;
        e.target.setAttribute("data-page", paged);

        if (paged <= totalPages) {
          const search =
            history.state && history.state.search ? history.state.search : "";
          loadPosts({
            taxonomy,
            pageNo: paged,
            search,
          });
        }
      });
    });
  }

  function loadPosts(filters, isPrefetch) {
    const { search, taxonomy, pageNo } = filters;

    if (!isPrefetch) {
      history.pushState(
        {
          search,
          taxonomy,
          pageNo,
        },
        ""
      );

      // Sync active states
      if (categoryButtons.length) {
        for (let i = 0; i < categoryButtons.length; i++) {
          if (categoryButtons[i].getAttribute("data-term-id") === taxonomy) {
            categoryButtons[i].classList.add("active");
          } else {
            categoryButtons[i].classList.remove("active");
          }
        }
      }
      // Sync active states on pagination -- these will be rehydrated once fetch is completed
      // const paginationButtons = document.querySelectorAll(".pagination-page");
      // if (paginationButtons.length) {
      //   for (let i = 0; i < paginationButtons.length; i++) {
      //     if (
      //       paginationButtons[i].getAttribute("data-page") &&
      //       paginationButtons[i].getAttribute("data-page") === pageNo
      //     ) {
      //       if (paginationButtons[i].classList.contains("pagination-arrow")) {
      //         paginationButtons[i].disabled = true;
      //       } else {
      //         paginationButtons[i].classList.add("current");
      //       }
      //     } else {
      //       if (paginationButtons[i].classList.contains("pagination-arrow")) {
      //         paginationButtons[i].disabled = false;
      //       } else {
      //         paginationButtons[i].classList.remove("current");
      //       }
      //     }
      //   }
      // }
    }

    // Check if we have cache
    const cachedResults = window._devotionalImagesData.find(
      (data) =>
        data.filters.search === search &&
        data.filters.taxonomy === taxonomy &&
        data.filters.pageNo === pageNo
    );
    if (cachedResults) {
      if (!isPrefetch) {
        populatePosts(cachedResults.results, filters);
      }
      return;
    }

    if (!isPrefetch) {
      removeChildren(postsWrapper);
      postsWrapper.insertAdjacentHTML("beforeend", placeholders);
    }

    // Build REST URL

    let url = "";
    let params = "";
    params += taxonomy ? "&devotional_image_category=" + taxonomy : "";
    params += pageNo ? "&page=" + pageNo : "";
    params += search ? "&s=" + search : "";

    if (search) {
      url =
        siteData.restUrl +
        "relevanssi/v1/search?post_type=devotional_image&_embed&_fields=id,title,date,acf,_embedded,_links&per_page=" +
        perPage +
        params;
    } else {
      url =
        siteData.restUrl +
        "wp/v2/devotional_image?_embed&_fields=id,title,date,acf,_embedded,_links&per_page=" +
        perPage +
        params;
    }

    $.ajax({
      url: url,
      type: "GET",
      dataType: "json",
      success: function (data, textStatus, jqXHR) {
        var results = {
          data: data,
          wpTotalPages: parseInt(
            jqXHR.getResponseHeader("X-WP-TotalPages"),
            10
          ),
        };

        if (!isPrefetch) {
          populatePosts(results, filters);
        }

        // Save to cache
        window._devotionalImagesData.push({
          filters: Object.assign({}, filters),
          results,
        });
      },
      error: function () {
        if (!isPrefetch) {
          // console.error("error loading posts");
          var results = {
            data: null,
            wpTotalPages: 0,
          };
          populatePosts(results, filters);
        }
      },
    });
  }

  function handleSearchInput() {
    const inputs = document.querySelectorAll("input[name='search-devotional']");
    if (!inputs.length) return;

    let typingTimer;
    let doneTypingInterval = 500;

    for (let i = 0; i < inputs.length; i++) {
      inputs[i].addEventListener("keyup", function (e) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function () {
          if (!history.state || history.state.search !== e.target.value) {
            loadPosts({
              taxonomy: "",
              pageNo: 1,
              search: e.target.value,
            });
          }
        }, doneTypingInterval);
      });
    }
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    const input = e.currentTarget.querySelector(
      "input[name='search-devotional']"
    );
    if (!input.length) return;

    const search = input.value;

    loadPosts({
      taxonomy: "",
      pageNo: 1,
      search,
    });
  }

  function searchCategory(filter) {
    for (let i = 0; i < categoryListItems.length; i++) {
      const txtValue =
        categoryListItems[i].textContent || categoryListItems[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
        categoryListItems[i].style.display = "";
      } else {
        categoryListItems[i].style.display = "none";
      }
    }
  }

  function handleCategorySearchInputKeyup(e) {
    const value = e.currentTarget.value;
    searchCategory(value);
  }

  function handleCategorySearchFormSubmit(e) {
    e.preventDefault();
    searchCategory(categorySearchInput.value);
  }

  function handleCategoryButtonClick(e) {
    e.preventDefault();
    const termID = e.currentTarget.getAttribute("data-term-id");

    loadPosts({
      taxonomy: termID,
      pageNo: 1,
      search: history.state && history.state.search ? history.state.search : "",
    });
  }

  // function startSearch(start = true) {
  //   const tabBodies = document.querySelectorAll(".tab-body");
  //   [].forEach.call(tabBodies, function (tb) {
  //     if (!start) {
  //       tb.querySelector(
  //         ".tab-body-wrapper:not(.tab-body-wrapper--search)"
  //       ).classList.remove("hidden");
  //       tb.querySelector(".tab-body-wrapper--search").classList.remove(
  //         "displayed"
  //       );
  //     } else {
  //       tb.querySelector(
  //         ".tab-body-wrapper:not(.tab-body-wrapper--search)"
  //       ).classList.add("hidden");
  //       tb.querySelector(".tab-body-wrapper--search").classList.add(
  //         "displayed"
  //       );
  //     }
  //   });

  //   // console.log(tabContents);
  // }

  $(document).on("lity:open", function (event, instance) {
    if (
      instance.opener().attr("href") !== "#devotional-modal" &&
      instance.opener().attr("href") !== "#devotional-downloads"
    ) {
      return;
    }
    var id = instance.opener().data("id");
    var isDownloadsModal =
      instance.opener().attr("href") === "#devotional-downloads";
    var params = isDownloadsModal
      ? "?_fields=id,acf"
      : "?_fields=id,title,date,acf";

    $.ajax({
      url: siteData.restUrl + "wp/v2/devotional_image/" + id + params,
      type: "GET",
      dataType: "json",
      success: function (data, textStatus, jqXHR) {
        var results = { data: data };
        if (isDownloadsModal) {
          populateDownloadsData(results.data);
        } else {
          populateData(results.data);
        }
      },
      error: function () {
        console.error("error loading posts");
      },
    });

    function populateData(data) {
      const lang = getDocumentLanguage();
      const titleElement = document.getElementById("devotional-modal-title");
      const title =
        lang !== "en" && data.acf.title && data.acf.title[lang]
          ? data.acf.title[lang]
          : data.title.rendered;
      titleElement.innerHTML = title;

      const date = document.getElementById("devotional-modal-date");
      date.innerHTML = formatDate(data.date);

      const desc = document.getElementById("devotional-modal-desc");
      const descriptions =
        data.acf && data.acf.descriptions ? data.acf.descriptions : "";
      if (descriptions) {
        let descHTML = "<ul>";
        Object.keys(descriptions).map(function (key) {
          let langStr = "";
          let text = descriptions[key].text;
          let link = descriptions[key].link;
          text += link
            ? " " +
              '<a href="' +
              link +
              '" target="_blank" rel="noopener noreferrer">' +
              link +
              "</a>"
            : "";

          switch (key) {
            case "en":
              langStr = "English";
              break;
            case "es":
              langStr = "Español";
              break;
            case "it":
              langStr = "Italiano";
              break;
            case "fr":
              langStr = "Français";
              break;
            case "pt":
              langStr = "Português";
              break;
          }

          descHTML +=
            '<li><span class="lang">' +
            langStr +
            ":</span>" +
            " " +
            text +
            "</li>";
        });
        descHTML += "</ul>";
        desc.innerHTML = descHTML;
      } else {
        desc.innerHTML = "";
      }

      // const download = document.getElementById('devotional-modal-download');
      // download.innerHTML = data.acf && data.acf.download_link ? '<a class="btn" href="' + data.acf.download_link + '" target="_blank" rel="noopener noreferrer"><svg xmlns="http://www.w3.org/2000/svg" width="20.053" height="18.337" viewBox="0 0 20.053 18.337"><g transform="translate(1 0)"><path d="M160,2787.015v6.171h18.053v-6.171" transform="translate(-160 -2775.849)" fill="none" stroke="#9a6d32" stroke-width="2"/><g transform="translate(4.831 0)"><path d="M142.485,2792.187l4.075,4.079,4.075-4.079" transform="translate(-142.485 -2782.633)" fill="none" stroke="#9a6d32" stroke-width="2"/><path d="M146.112,2790.982v-13.633" transform="translate(-142.037 -2777.349)" fill="none" stroke="#9a6d32" stroke-width="2"/></g></g></svg><span>' + downloadStr + '</span></a>' : '';

      setPlaceholder(false);
    }

    function populateDownloadsData(data) {
      var links = document.getElementById("devotional-downloads-links");
      var images = data.acf.images;
      var downloadLink = data.acf.download_link;
      var downloadIcon =
        '<svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.1111 8V14.2222H1.88889V8H0V14.2222C0 15.2 0.85 16 1.88889 16H15.1111C16.15 16 17 15.2 17 14.2222V8H15.1111ZM9.44444 8.59556L11.8906 6.30222L13.2222 7.55556L8.5 12L3.77778 7.55556L5.10944 6.30222L7.55556 8.59556V0H9.44444V8.59556Z" fill="currentColor"/></svg>';
      var assetsDir = "/en/wp-content/themes/bridge-child/orientale-v2/assets";

      var html = "";
      html += "<ul>";

      if (
        images &&
        !Object.values(images).every((img) => img === null || img === "")
      ) {
        for (var key in images) {
          if (images.hasOwnProperty(key) && images[key]) {
            var link = images[key];
            var title;

            switch (key) {
              case "linkedin":
                title = "LinkedIn";
                break;
              default:
                title = key.charAt(0).toUpperCase() + key.slice(1);
                break;
            }
            html +=
              '<li><a href="' +
              link +
              '" target="_blank" rel="noopener noreferrer"><img src="' +
              assetsDir +
              "/icons/" +
              key +
              ".svg" +
              '">' +
              "<span>" +
              title +
              "</span>" +
              downloadIcon +
              "</a></li>";
          }
        }
      } else if (downloadLink) {
        html +=
          '<li><a href="' +
          downloadLink +
          '" target="_blank" rel="noopener noreferrer">' +
          "<span>All Platforms</span>" +
          downloadIcon +
          "</a></li>";
      }
      html += "</ul>";
      links.innerHTML = html;
      setPlaceholder(false);
    }
  });

  $(document).on("lity:remove", function (event, instance) {
    setPlaceholder(true);
  });

  function setPlaceholder(active = true) {
    if (active) {
      document
        .getElementById("devotional-modal")
        .classList.add("devotional__modal--placeholder");

      document
        .getElementById("devotional-downloads")
        .classList.add("devotional__modal--placeholder");
    } else {
      document
        .getElementById("devotional-modal")
        .classList.remove("devotional__modal--placeholder");

      document
        .getElementById("devotional-downloads")
        .classList.remove("devotional__modal--placeholder");
    }
  }
}

// HELPER FUNCTIONS

function getDocumentLanguage() {
  let lang = "en";
  switch (document.getElementsByTagName("html")[0].getAttribute("lang")) {
    case "it-IT":
    case "it":
      lang = "it";
      break;
    case "es-ES":
    case "es":
      lang = "es";
      break;
    default:
      lang = "en";
  }
  return lang;
}

function formatDate(date) {
  const dateObject = new Date(date);
  const options = { year: "numeric", month: "short", day: "numeric" };
  const lang = getDocumentLanguage();
  let locale = "en-US";
  switch (lang) {
    case "es":
      locale = "es-ES";
      break;
    case "it":
      locale = "it-IT";
      break;
    default:
      locale = "en-US";
  }
  return dateObject.toLocaleDateString(locale, options);
}

function removeChildren(el) {
  while (el.firstChild) {
    el.removeChild(el.lastChild);
  }
}

// MOBILE MENU CONTROL
$j(document).ready(function () {
  $j(".mobile_menu_button>span").click(function () {
   $j(".mobile_menu #menu-main-menu-1").toggleClass("active");
  });
 });
 
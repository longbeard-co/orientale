<?php
/*
Template Name: Devotional Images
*/
?>

<?php
$bridge_qode_id = bridge_qode_get_page_id();
$bridge_qode_sidebar = get_post_meta($bridge_qode_id, "qode_show-sidebar", true);

$bridge_qode_enable_page_comments = false;
if (get_post_meta($bridge_qode_id, "qode_enable-page-comments", true) == 'yes') {
  $bridge_qode_enable_page_comments = true;
}

if (get_post_meta($bridge_qode_id, "qode_page_background_color", true) != "") {
  $bridge_qode_background_color = get_post_meta($bridge_qode_id, "qode_page_background_color", true);
} else {
  $bridge_qode_background_color = "";
}

$bridge_qode_content_style_spacing = "";
if (get_post_meta($bridge_qode_id, "qode_margin_after_title", true) != "") {
  if (get_post_meta($bridge_qode_id, "qode_margin_after_title_mobile", true) == 'yes') {
    $bridge_qode_content_style_spacing = "padding-top:" . esc_attr(get_post_meta($bridge_qode_id, "qode_margin_after_title", true)) . "px !important";
  } else {
    $bridge_qode_content_style_spacing = "padding-top:" . esc_attr(get_post_meta($bridge_qode_id, "qode_margin_after_title", true)) . "px";
  }
}

if (get_query_var('paged')) {
  $bridge_qode_paged = get_query_var('paged');
} elseif (get_query_var('page')) {
  $bridge_qode_paged = get_query_var('page');
} else {
  $bridge_qode_paged = 1;
}

?>
<?php get_header(); ?>
<?php get_template_part('title'); ?>
<?php
$bridge_qode_revslider = get_post_meta($bridge_qode_id, "qode_revolution-slider", true);
if (!empty($bridge_qode_revslider)) { ?>
  <div class="q_slider">
    <div class="q_slider_inner">
      <?php echo do_shortcode($bridge_qode_revslider); ?>
    </div>
  </div>
<?php
}
?>
<div class="full_width" <?php if ($bridge_qode_background_color != "") {
                          echo " style='background-color:" . $bridge_qode_background_color . "'";
                        } ?>>
  <div class="full_width_inner" <?php bridge_qode_inline_style($bridge_qode_content_style_spacing); ?>>
    <?php
    // $taxonomies = get_object_taxonomies('devotional_image');
    // $tax = $taxonomies[0];

    $terms = get_terms(array(
      'taxonomy' => 'devotional_image_category',
    ));

    $locale = get_locale();
    ?>
    <div id="devotional-images" class="vc_row wpb_row section vc_row-fluid devotional-images">
      <div class=" full_section_inner clearfix">
        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-1 vc_col-lg-10">
          <div class="vc_column-inner ">
            <div class="devotional-images__title">
              <h2 style="text-align:left;"><?php the_title(); ?></h2>
              <?php echo wp_get_attachment_image(5874, 'large', '', ['class' => 'title-img']); // Image ID on EN (Prod) 
              ?>
              <?php
              switch ($locale):
                case 'it_IT':
                  $search_str = 'Cerca immagini';
                  $category_search_str = 'Cerca categorie';
                  break;
                default:
                  $search_str = 'Search Images';
                  $category_search_str = 'Search Categories';
              endswitch;
              ?>
              <div class="devotional-images__header devotional-images__header--sm">
                <div class="posts-search-wrapper">
                  <form id="posts-search-sm" class="posts-search search-wrapper">
                    <input id="posts-search-input-sm" type="text" placeholder="<?php echo $search_str; ?>" name="search-devotional" />
                    <button type="submit" aria-label="<?php echo $search_str; ?>"><i class="qode_icon_font_awesome fa fa-search "></i></button>
                  </form>
                </div>
                <div class="posts-pagination"></div>
              </div>
            </div>
            <div class="devotional-images__inner">
              <main>
                <div class="devotional-images__header devotional-images__header--lg">
                  <div class="posts-search-wrapper">
                    <form id="posts-search" class="posts-search search-wrapper">
                      <input id="posts-search-input" type="text" placeholder="<?php echo $search_str; ?>" name="search-devotional" />
                      <button type="submit" aria-label="<?php echo $search_str; ?>"><i class="qode_icon_font_awesome fa fa-search "></i></button>
                    </form>
                  </div>
                  <div class="posts-pagination"></div>
                </div>
                <div id="posts-wrapper" class="posts-wrapper"></div>
                <div class="devotional-images__footer">
                  <div class="posts-pagination"></div>
                </div>

              </main>
              <aside class="sidebar-lg">
                <?php if ($terms && !is_wp_error($terms)) : ?>
                  <div id="devotional-images-categories" class="devotional-images__categories">
                    <h3 class="devotional-images__categories__title">Categories</h3>
                    <form id="category-search-form" class="category-search search-wrapper">
                      <input id="category-search-input" type="text" placeholder="<?php echo $category_search_str; ?>" name="search-devotional-category" />
                      <button type="submit" aria-label="<?php echo $category_search_str; ?>"><i class="qode_icon_font_awesome fa fa-search "></i></button>
                    </form>
                    <div class="devotional-images__categories__list">
                      <ul>
                        <li>
                          <button type="button" class="btn" data-term-id="">
                            All
                          </button>
                        </li>
                        <?php foreach ($terms as $term) : ?>
                          <li>
                            <button type="button" class="btn" data-term-id="<?php echo $term->term_id; ?>">
                              <?php echo $term->name; ?>
                            </button>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>
                  </div>
                <?php endif; ?>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
switch (get_locale()):
  case 'it_IT':
    $date_str = 'Data';
    break;
  case 'es_ES':
    $date_str = 'Fecha';
    break;
  default:
    $date_str = 'Date';
endswitch;
?>
<div id="devotional-modal" class="devotional__modal devotional__modal--placeholder lity-hide">
  <!-- <button class="devotional__modal__close" data-lity-close>&times;</button> -->
  <h3 id="devotional-modal-title" class="devotional__title"></h3>
  <h4 class="devotional__date"><?php echo $date_str; ?>: <span id="devotional-modal-date"></span></h4>
  <div id="devotional-modal-desc" class="devotional__modal__desc"></div>
  <!-- <div id="devotional-modal-download" class="devotional__buttons"></div> -->
</div>
<div id="devotional-downloads" class="devotional__modal devotional__modal--downloads devotional__modal--placeholder lity-hide">
  <div class="devotional-downloads">
    <div class="devotional-downloads__links">
      <h5 class="bottom">Download the image for:</h5>
      <div id="devotional-downloads-links">
        <ul>
          <li><a></a></li>
          <li><a></a></li>
          <li><a></a></li>
          <li><a></a></li>
        </ul>
      </div>
    </div>
    <div class="devotional-downloads__subscribe">
      <h4 class="bottom">Subscribe</h4>
      <?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]'); ?>
    </div>
  </div>
</div>
<?php // End content 
?>

<?php do_action('bridge_qode_action_page_after_container') ?>
<?php get_footer(); ?>
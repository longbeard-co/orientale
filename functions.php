<?php

/**
 * Enable custom maintenance page
 * Located in Beardbalm theme folder: maintenance.php
 */
function wp_maintenance_mode() {
	// Kevin's IP Address Check
	$allowed_ip = '189.203.187.19';
	// $allowed_ip = '000.000.000.00';
	$ip = $_SERVER['REMOTE_ADDR'];

	if (!is_user_logged_in() && !preg_match("/$allowed_ip/", $ip)) {
		if (file_exists(__DIR__ . '/maintenance.php')) {
			include_once get_stylesheet_directory() . '/maintenance.php';
			die();
		}
	}
}
// add_action('wp', 'wp_maintenance_mode');

// enqueue the child theme stylesheet

function wp_lb_enqueue_scripts() {
	$styleWP = get_stylesheet_directory_uri() . '/style.css';
	$style_v2_WP = get_stylesheet_directory_uri() . '/orientale-v2/style.css';
	$stylePath = __DIR__ . '/style.css';
	$stylev2Path = __DIR__ . '/orientale-v2/style.css';
	$filetime = filemtime($stylePath);
	$filetime_v2 = filemtime($stylev2Path);

	wp_enqueue_style('child_style', $styleWP, array(), $filetime);
	wp_enqueue_style('child_stylev2', $style_v2_WP, array(), $filetime_v2);

	// SCRIPTS

	$jsWP = get_stylesheet_directory_uri() . '/orientale-v2/js/lb_custom.js';
	$jsPath = __DIR__ . '/orientale-v2/js/lb_custom.js';
	$filetimeJS = filemtime($jsPath);
	wp_enqueue_script('lb_custom-js', $jsWP, array('jquery'), $filetimeJS, true);
	wp_enqueue_script('slick-js', get_stylesheet_directory_uri() . '/orientale-v2/js/vendors/slick.min.js', array('jquery'), '1.0', true);

	if (is_home() || is_front_page() || is_page_template('devotional-images.php')) {
		wp_enqueue_script('lity', get_stylesheet_directory_uri() . '/orientale-v2/js/vendors/lity.min.js', array('jquery'), '', true);
	}

	wp_localize_script('lb_custom-js', 'siteData', array(
		'nonce' => wp_create_nonce('wp_rest'),
		'siteUrl' => (is_multisite() && !is_main_site()) ? get_home_url(get_main_site_id()) : get_home_url(),
		'restUrl' => get_rest_url(get_current_blog_id()),
	));
};

add_action('wp_enqueue_scripts', 'wp_lb_enqueue_scripts', 11);


function mytheme_add_woocommerce_support() {
	add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

// Add Custom Search Shortcode -- EH
add_shortcode('wpbsearch', 'get_search_form');

// Anonymize form entries
add_filter('gform_ip_address', '__return_empty_string');

// Add Custom Category as class to body -- EH
// add_filter( 'body_class', 'atp_custom_taxonomy_body_class', 10, 3 );

if (!function_exists('atp_custom_taxonomy_body_class')) {
	function atp_custom_taxonomy_body_class($classes, $class, $ID) {
		$taxonomies_args = array(
			'public' => true,
			'_builtin' => false,
		);
		$taxonomies = get_taxonomies($taxonomies_args, 'names', 'and');
		$terms = get_the_terms((int) $ID, (array) $taxonomies);
		if (!empty($terms)) {
			foreach ((array) $terms as $order => $term) {
				if (!in_array($term->slug, $classes)) {
					$classes[] = $term->slug;
				}
			}
		}
		$classes[] = 'clearfix';

		return $classes;
	}
}

// LB CPT
require_once('lb-cpt.php');


// ADD LANGUAGE NOTICE FOR LB TEAM -- EH
function my_error_notice() {
?>
	<div class="error notice">
		<p><strong><?php _e('THIS IS THE ENGLISH SITE 🇬🇧', 'my_plugin_textdomain'); ?></strong></p>
	</div>
<?php
}
add_action('admin_notices', 'my_error_notice');

// remove woocommerce scripts on unnecessary pages
function woocommerce_de_script() {
	if (function_exists('is_woocommerce')) {
		if (!is_woocommerce() && !is_cart() && !is_checkout() && !is_account_page()) { // if we're not on a Woocommerce page, dequeue all of these scripts
			wp_dequeue_script('wc-add-to-cart');
			wp_dequeue_script('jquery-blockui');
			wp_dequeue_script('jquery-placeholder');
			wp_dequeue_script('woocommerce');
			wp_dequeue_script('jquery-cookie');
			wp_dequeue_script('wc-cart-fragments');
		}
	}
}
add_action('wp_print_scripts', 'woocommerce_de_script', 100);
add_action('wp_enqueue_scripts', 'remove_woocommerce_generator', 99);
function remove_woocommerce_generator() {
	if (function_exists('is_woocommerce')) {
		if (!is_woocommerce()) { // if we're not on a woo page, remove the generator tag
			remove_action('wp_head', array($GLOBALS['woocommerce'], 'generator'));
		}
	}
}
// remove woocommerce styles, then add woo styles back in on woo-related pages
function child_manage_woocommerce_css() {
	if (function_exists('is_woocommerce')) {
		if (!is_woocommerce()) { // this adds the styles back on woocommerce pages. If you're using a custom script, you could remove these and enter in the path to your own CSS file (if different from your basic style.css file)
			wp_dequeue_style('woocommerce-layout');
			wp_dequeue_style('woocommerce-smallscreen');
			wp_dequeue_style('woocommerce-general');
		}
	}
}
add_action('wp_enqueue_scripts', 'child_manage_woocommerce_css');

// Remove duplicates from feed

add_filter('the_title', 'track_displayed_posts');
add_action('pre_get_posts', 'remove_already_displayed_posts');
$displayed_posts = [];
function track_displayed_posts($title) {
	global $displayed_posts;
	$displayed_posts[] = get_the_ID();
	return $title; // don't mess with the title
}

function remove_already_displayed_posts($query) {
	global $displayed_posts;
	if (!is_admin() && isset($query->query['location']) && $query->query['location'] != 'sidebar' && !is_archive()) {
		$query->set('post__not_in', $displayed_posts);
	}
}

function v2_enqueue_scripts() {
	wp_register_style('childstyle-v2', get_stylesheet_directory_uri() . '/orientale-v2/style.css');
	wp_enqueue_style('childstyle-v2');
}

function year_archive_func($atts) {
	$url = $_SERVER['REQUEST_URI']; //returns the current URL
	$parts = explode('/', $url);
	$acceptLang = ['it', 'en'];
	$lang = in_array($parts[1], $acceptLang) ? $parts[1] : 'en';

	$a = shortcode_atts(array(
		'category_name' => '',
		'offset' => 0
	), $atts);

	ob_start();
?>
	<div class="years-col">
		<div class="years-col-inner">
			<?php
			$lang = get_locale();
			if ($lang == 'en_US') {
				echo '<h2>Archive</h2>';
			} else {
				echo '<h2>Archivio</h2>';
			}
			?>
			<div class="years-wrapper">
				<?php
				$args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 9999,
					'ignore_sticky_posts' => true,
					'category_name' => $a['category_name'],
					'offset' => $a['offset'],
					'location' => 'sidebar', // excluded by pre_get_posts
				);

				$qry = new WP_Query($args);

				$yearsArray = array();
				foreach ($qry->posts as $p) {
					array_push($yearsArray, get_the_date('Y', $p->ID));
				}
				$uniqueYears = array_unique($yearsArray);
				foreach ($uniqueYears as $year) {
					if ($lang == 'en_US') {
						if ($a && $a['category_name']) {
							$href = '/en/category/' . $a['category_name'] . '/year/' . $year . '/';
						} else {
							$href = '/en/' . $year . '/';
						}
				?>
						<a href="<?php echo $href ?>"><?php echo $year ?></a>
					<?php } else {
						if ($a && $a['category_name']) {
							$href = '/it/category/' . $a['category_name'] . '/year/' . $year . '/';
						} else {
							$href = '/it/' . $year . '/';
						}
					?>
						<a href="<?php echo $href ?>"><?php echo $year ?></a>
				<?php }
				}
				?>
			</div>
		</div>
	</div>
<?php
	return ob_get_clean();
}

add_shortcode('year_archive', 'year_archive_func');

function archive_dropdown() {
	ob_start();
?>

	<div class="blog-list-archive">

		<?php

		$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date)
	FROM $wpdb->posts WHERE post_status = 'publish'
	AND post_type = 'post' ORDER BY post_date DESC");
		foreach ($years as $year) :
		?>
			<li><a href="JavaScript:void()"><?php echo $year; ?></a>

				<ul class="archive-sub-menu">
					<?php $months = $wpdb->get_col("SELECT DISTINCT MONTH(post_date)
			FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post'
			AND YEAR(post_date) = '" . $year . "' ORDER BY post_date DESC");
					foreach ($months as $month) :
					?>
						<li><a href="<?php echo get_month_link($year, $month); ?>">

								<?php echo date('F', mktime(0, 0, 0, $month)); ?></a>

						</li>

					<?php endforeach; ?>

				</ul>

			</li>

		<?php endforeach; ?>

	</div>
<?php ob_get_clean();
}

add_shortcode('archive_dropdown_widget', 'archive_dropdown');

function latest_date_post($atts) {

	$a = shortcode_atts(array(
		'numToShow' => -1,
		'numToSkip' => 0,
	), $atts);

	$args = array(
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => $a['numToShow'],
		'offset' => $a['numToSkip'],
		'date_query' => array(
			array(
				'year'  => $wp_query->query_vars['year']
			),
		),
	);

	$q = new WP_Query($args);

	$html = "";
	$html .= "<div class='latest_post_two_holder $columns_number'>";
	$html .= "<ul>";

	while ($q->have_posts()) : $q->the_post();

		$html .= '<li class="clearfix"><a itemprop="url" href="' . get_permalink() . '">';
		if ($display_featured_images === "yes") {
			$html .= '<div class="latest_post_two_image">';
			$html .= '<a itemprop="url" href="' . get_permalink() . '">';

			if ($featured_image_size !== 'custom' || $image_width == '' || $image_height == '') {
				$html .= get_the_post_thumbnail(get_the_ID(), $image_size);
			} else {
				$html .= qode_generate_thumbnail(get_post_thumbnail_id(get_the_ID()), null, $image_width, $image_height);
			}
			$html .= '</a>';
			$html .= '</div>';
		}
		$html .= '<div class="latest_post_two_inner" ' . $holder_style . '>';

		$html .= '<div class="latest_post_two_text">';

		$html .= '<h5 itemprop="name" class="latest_post_two_title entry_title"><a itemprop="url" href="' . get_permalink() . '" ' . $title_style . '>' . get_the_title() . '</a></h5>';


		if ($text_length != '0') {
			$excerpt = ($text_length > 0) ? mb_substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt();
			$html .= '<div itemprop="dateCreated" class="post_info_date entry_date updated">' . get_the_time('d M Y') . '</div>';
		}

		$html .= '</div>';

		$html .= '</div></a>';
		$html .= '</li>';

	endwhile;


	$html .= "</ul></div>";

	echo $html;
	wp_reset_postdata();
}

add_shortcode('year_archive_posts', 'latest_date_post');

function lb_cookiebot() {
	$curLang = substr(get_locale(), 0, 2);
	$cookiebot = '
		<script 
			id="Cookiebot" 
			src="https://consent.cookiebot.com/uc.js" 
			data-cbid="a795b78c-37cd-491f-a5f6-ba64bd2f83f9" 
			data-culture="' . $curLang . '" 
			data-blockingmode="auto" 
			type="text/javascript"></script>
		<style>
			#CybotCookiebotDialogBodyContentTitle {
				font-size: 12pt !important;
			}
			@media screen and (max-width: 600px) {
				#CybotCookiebotDialogBodyLevelButtonsTable {
					width: 99% !important;
				}
			}
		</style>';

	echo "\n" . $cookiebot;
}

add_action('bridge_qode_action_header_meta', 'lb_cookiebot', 1);


function lb_analytics() {
	ob_start();
?>
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '519854255676269');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=519854255676269&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67479109-8"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('set', 'anonymizeIp', true);
		gtag('config', 'UA-67479109-8');
	</script>

<?php
	$code = ob_get_clean();
	echo "\n" . $code;
}

add_action('wp_head', 'lb_analytics');

// Language Switcher

function lb_language_switcher_func() {
	// language switcher
	global $post;
	if (get_field('linked_url') || get_field('linked_translation')) :
		// 1. Get from ACF
		$url = get_field('linked_url') ?:  get_field('linked_translation')['value'];
	else :
		// 2. Get from same page id
		$site_url = get_site_url();
		$host = parse_url($site_url, PHP_URL_HOST);
		$lang_url = (get_locale() && get_locale() == 'it_IT') ? $host . '/en' : $host . '/it';

		if (is_front_page()) {
			$url = $lang_url;
		} elseif ('portfolio_page' == get_post_type()) {
			$url = $lang_url . '/profile/' . $post->post_name;
		} else {
			$url = $lang_url . '/index.php?p=' . get_the_ID();
		}
	endif;

	$flag_en = '<svg xmlns="http://www.w3.org/2000/svg" width="1000" height="600" viewBox="0 0 50 30"  aria-labelledby="flag-en" role="img" ><title id="flag-en">English</title><clipPath id="t"><path d="M25,15 h25 v15 z v15 h-25 z h-25 v-15 z v-15 h25 z"/></clipPath><path d="M0,0 v30 h50 v-30 z" fill="#00247d"/><path d="M0,0 L50,30 M50,0 L0,30" stroke="#fff" stroke-width="6"/><path d="M0,0 L50,30 M50,0 L0,30" clip-path="url(#t)" stroke="#cf142b" stroke-width="4"/><path d="M25,0 v30 M0,15 h50" stroke="#fff" stroke-width="10"/><path d="M25,0 v30 M0,15 h50" stroke="#cf142b" stroke-width="6"/></svg>';
	$flag_it = '<svg xmlns="http://www.w3.org/2000/svg" width="1500" height="1000" viewBox="0 0 3 2"  aria-labelledby="flag-it" role="img" ><title id="flag-it">Italiano</title><rect fill="#009246" height="2" width="1" /><rect fill="#f1f2f1" height="2" width="1" x="1" /><rect fill="#ce2b37" height="2" width="1" x="2" /></svg>';

	$html = '<div class="language-switch language-switch-php">';
	$html .= '<button class="language-switch__button flag">';
	$html .= (get_locale() && get_locale() == 'it_IT') ? $flag_it : $flag_en;
	$html .= '<span class="language-switch__arrow"><svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.41 0L6 4.58L10.59 0L12 1.41L6 7.41L0 1.41L1.41 0Z" fill="currentColor"/></svg></span>';
	$html .= '</button>';
	$html .= '<div class="language-switch__dropdown">';
	$html .= '<a class="language-switch__link flag" href="' . esc_url($url) . '">';
	$html .= (get_locale() && get_locale() == 'it_IT') ? $flag_en : $flag_it;
	$html .= '</a>';
	$html .= '</div>';
	$html .= '</div>';

	return $html;
}

// add_action('wp_body_open', 'lb_language_switcher');
add_shortcode('lb_language_switcher', 'lb_language_switcher_func');

function lb_header_top_menu_func() {
	return wp_nav_menu(array(
		'menu' => 'top-header-menu',
		'echo' => false,
	));
}
add_shortcode('lb_header_top_menu', 'lb_header_top_menu_func');

function lb_header_top_sign_in_func() {
	return '<a href="https://mail.google.com/a/orientale.it" target="_blank" rel="noopener noreferrer">
		<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14"><path fill="#fff" d="M15.3 0H1.7C1.25.002.82.188.503.516A1.777 1.777 0 0 0 .008 1.75L0 12.25c.001.464.18.908.5 1.236.318.328.75.513 1.2.514h13.6a1.68 1.68 0 0 0 1.2-.514c.32-.328.499-.772.5-1.236V1.75a1.782 1.782 0 0 0-.5-1.236A1.68 1.68 0 0 0 15.3 0Zm0 3.5L8.5 7.875 1.7 3.5V1.75l6.8 4.375 6.8-4.375V3.5Z"/></svg>
		Sign in
	</a>';
}
add_shortcode('lb_header_top_sign_in', 'lb_header_top_sign_in_func');

function lb_social_func() {
	return '<div class="social">
	<span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" ">
		<a itemprop="url" href="https://www.facebook.com/PontificioIstitutoOrientale/" target="_blank" rel="noopener">
			<i class="qode_icon_font_awesome fa fa-facebook qode_icon_element" style=""></i>
		</a>
	</span>
	<span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" ">
		<a itemprop="url" href="https://twitter.com/theorientale" target="_blank" rel="noopener">
			<i class="qode_icon_font_awesome fa fa-twitter qode_icon_element" style=""></i>
		</a>
	</span>
	<span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" ">
		<a itemprop="url" href="https://www.youtube.com/theorientale" target="_blank" rel="noopener">
		<i class="qode_icon_font_awesome fa fa-youtube-play qode_icon_element" style=""></i>
		</a>
	</span>
	<span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" ">
		<a itemprop="url" href="https://www.linkedin.com/company/pontifical-oriental-institute" target="_blank" rel="noopener">
			<i class="qode_icon_font_awesome fa fa-linkedin qode_icon_element" style=""></i>
		</a>
	</span>
	<span data-type="normal" class="qode_icon_shortcode  q_font_awsome_icon fa-2x  " style=" ">
		<a itemprop="url" href="https://www.instagram.com/theorientale/" target="_blank" rel="noopener">
			<i class="qode_icon_font_awesome fa fa-instagram qode_icon_element" style=""></i>
		</a>
	</span>
	</div>';
}
add_shortcode('lb_social', 'lb_social_func');

function lb_header_search_func() {
	return '<div id="header-search" class="header-search">
			<button id="header-search-trigger" type="button" aria-label="Search" class="header-search__trigger">
				<i class="qode_icon_font_awesome fa fa-search "></i>
			</button>
			<div id="header-search-dropdown" class="header-search__dropdown">
				<form action="' . home_url('/') . '" method="GET">
					<input type="search" name="s" id="header-search-input" placeholder="Search ..." />
					<button type="submit" aria-label="Search"><i class="qode_icon_font_awesome fa fa-search "></i></button>
				</form>
			</div>
		</div>
		';
}
add_shortcode('lb_header_search', 'lb_header_search_func');

// ACF Language Switcher

function acf_load_posts_in_another_language($field) {

	// reset choices
	$field['choices'] = array();

	// get choices
	$site_url = get_site_url();
	$host = parse_url($site_url, PHP_URL_HOST);
	$lang_url = (get_locale() == 'it_IT') ? $host . '/en' : $host . '/it';
	$post_type_name = 'posts';

	$url = 'https://' . $lang_url . '/wp-json/wp/v2/' . $post_type_name . '/?_fields=id,title.rendered,link&per_page=50';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	$result = curl_exec($ch);
	$posts = json_decode($result, true);

	// loop through array and add to field 'choices'
	if (is_array($posts)) {
		foreach ($posts as $p) {
			$title = $p['title']['rendered'];
			$link = $p['link'];
			$id = $p['id'];
			$field['choices'][$link] = $title . ' (' . $id . ')';
		}
	}

	// return the field
	return $field;
}

add_filter('acf/load_field/name=linked_translation', 'acf_load_posts_in_another_language');

// On Save Post Filter - Update Linked Translation
function my_acf_save_post($post_id) {

	if ('post' != get_post_type($post_id)) return;

	// Get translated site url
	$site_url = get_site_url();
	$host = parse_url($site_url, PHP_URL_HOST);
	$lang_url = (get_locale() == 'it_IT') ? $host . '/en' : $host . '/it';

	// Check the new value of a specific field.
	$this_url = get_the_permalink($post_id);
	$this_title = get_the_title($post_id);

	$linked_translation = get_field('linked_translation', $post_id);

	if ($linked_translation) {
		preg_match_all('#\((.*?)\)#', $linked_translation['label'], $match);
		$last_match = $match[sizeof($match) - 1];
		$linked_translation_id = $last_match[sizeof($last_match) - 1];

		if (strpos($host, 'longbeardco') !== false) {
			$app_user = 'orientale_admin';
			$app_pass = (get_locale() == 'it_IT') ? 'jfan gd6d uKOm YkdD foCH GIwg' : '8ntS mFbR Z7Od ckxH G2wd vaGi';
		} else {
			$app_user = 'rest-editor';
			$app_pass = (get_locale() == 'it_IT') ? 'QJlT xsJn ue3l Fqhh Qd14 RhNz' : 'qCD2 st8p aC3w GTau VVxm 1pwp';
		}

		$api_response = wp_remote_post(
			'https://' . $lang_url . '/wp-json/wp/v2/posts/' . $linked_translation_id . '/',
			[
				'headers' => [
					'Authorization' => 'Basic ' . base64_encode($app_user . ':' . $app_pass)
				],
				'body' => [
					'meta' => [
						'linked_translation' => ['value' => $this_url, 'label' => $this_title . ' (' . $post_id . ')'],
					]
				]
			]
		);
	}
}
add_action('acf/save_post', 'my_acf_save_post');

// Enable updating post from REST API
add_action("rest_insert_post", function (\WP_Post $post, $request, $creating) {
	$metas = $request->get_param("meta");
	if (is_array($metas)) {
		foreach ($metas as $name => $value) {
			update_post_meta($post->ID, $name, $value);
		}
	}
}, 10, 3);

add_action('init', 'wpse31422_init');
function wpse31422_init() {
	add_rewrite_endpoint('year', EP_CATEGORIES);
}


/* Change Open Graph URL in Yoast SEO for homepages
* Author: PW (LB)
 * Last Tested: 10.30.2020 | WP 5.1 | Yoast 14.8.1
 */


add_filter('wpseo_opengraph_url', 'change_opengraph_url');

function change_opengraph_url($url) {
	if (is_page(4529) || is_page(4741)) {
		$url = 'https://orientale.it/en/wp-content/uploads/2020/11/Orientale-Web-Grab-1200x630-1-1.gif';
	}
	return $url;
}

// Custom Translation

add_filter('gettext', 'lb_custom_translation', 20, 3);

function lb_custom_translation($translated_text, $text, $domain) {

	if ($domain === 'lb' || $domain === 'qode') {
		$locale = get_locale();

		$translations = [
			'Latest' => [
				'it_IT' => 'Notizie',
			],
			'View More' => [
				'it_IT' => 'Mostra di più',
			],
			'Search Website' => [
				'it_IT' => 'Cerca nel sito'
			],
			'Study at the Orientale' => [
				'it_IT' => 'Studiare all’Orientale',
			],
			'Stable Professors' => [
				'it_IT' => 'Professori Stabili',
			],
			'Professors' => [
				'it_IT' => 'Professori',
			],
			'News' => [
				'it_IT' => 'Notizie',
			],
			'Events' => [
				'it_IT' => 'Eventi',
			],
			"Deans' Blog" => [
				'it_IT' => 'Blog di decani',
			],
			'Important Events' => [
				'it_IT' => 'Eventi importanti',
			],
			'View Full Calendar' => [
				'it_IT' => 'Visualizza il calendario completo',
			],
			'No posts' => [
				'it_IT' => 'Nessun post',
			],
			'Featured Publication' => [
				'it_IT' => 'Pubblicazioni in evidenza',
			],
			'Support the future of Eastern Christianity' => [
				'it_IT' => 'Come Investire nel futuro delle Chiese Orientali',
			],
			// Donate Page
			'Donate' => [
				'it_IT' => 'Donazioni'
			],
			"We're Independent" => [
				'it_IT' => 'Siamo Indipendenti',
			],
			'Although the Oriental Institute forms the Gregorian Consortium with the Gregorian University and the Biblical Institute, it is academically and financially independent from them.' => [
				'it_IT' => "Benchè l'Istituto Orientale sia membro del Consorzio Gregoriano con l'Università Gregoriana e l'Istituto Biblico, esso è accademicamente e finanziariamente indipendente.",
			],
			'Affordable Education' => [
				'it_IT' => 'Formazione Accessibile',
			],
			'Help us keep the cost of studies low to provide accessibility to students travelling from poor countries.' => [
				'it_IT' => "Aiutaci a mantenere bassi i costi degli studi per consentire l'accessibilità agli studenti che arrivano da paesi poveri.",
			],
			'We Need Partners' => [
				'it_IT' => 'Cerchiamo Partner',
			],
			'Your help is needed for us to carry out our work at the service of the Church. Some church institutions support us, but we look to our benefactors to assist us in meeting our financial needs.' => [
				'it_IT' => "Il tuo aiuto è necessario per compiere il nostro lavoro al servizio della Chiesa. Alcune istituzioni ecclesiastiche ci sostengono, ma ci rivolgiamo ai nostri benefattori per aiutarci a sostenere i nostri bisogni economici.",
			],
			'Give Now' => [
				'it_IT' => 'Dona ora',
			],
			'Online' => [
				'it_IT' => 'Online',
			],
			'By Mail' => [
				'it_IT' => 'Per Posta',
			],
			'Italy & International' => [
				'it_IT' => 'Italia & Internazionale'
			],
			'United States' => [
				'it_IT' => 'Stati Uniti'
			],
			'Address' => [
				'it_IT' => 'Indirizzo'
			],
			'Phone' => [
				'it_IT' => 'Telefono'
			],
			'Email' => [
				'it_IT' => 'Email'
			],
			'The Oriental Institute gratefully accepts donations, which are tax deductible in the United States, through The Gregorian University Foundation. Donations to the Pontifical Oriental Institute need to mention the name of the Institute. Your generosity to our work is deeply appreciated.' => [
				'it_IT' => 'L’Istituto Orientale accetta con gratitudine donazioni, che sono deducibili negli Stati Uniti, attraverso The Gregorian University Foundation (Fondazione Gregoriana). Le donazioni al Pontificio Istituto Orientale necessitano della menzione del nome dell’istituto. La vostra generositá per il nostro lavoro é profondamente gradita.',
			]
		];

		if (isset($translations[$text]) && isset($translations[$text][$locale])) {
			$translated_text = $translations[$text][$locale];
		}
	}

	return $translated_text;
}

add_filter('gform_other_choice_value', 'set_placeholder', 10, 2);
function set_placeholder($placeholder, $field) {
	return get_locale() == 'it_IT' ? 'Altro importo' : 'Own Amount';
}

/**
 * Gravity Forms + Currency Selector + Stripe
 */

// 1. Modify Stripe payment intent currency
// add_filter('gform_stripe_session_data', function ($session_data, $feed, $submission_data, $form, $entry) {

// 	if (false !== strpos($form['cssClass'], 'donate-form')) { // set your form id.
// 		$currency = rgar($entry, '10') ?: 'USD';

// 		foreach ($session_data['line_items'] as $k => $line_item) {
// 			$session_data['line_items'][$k]['currency'] = $currency;
// 		}

// 		/**
// 		 * @see https://stripe.com/docs/payments/checkout/migrating-prices
// 		 */
// 		if (isset($session_data['subscription_data']) && isset($session_data['subscription_data']['items'])) {
// 			// if (!isset($session_data['line_items'])) {
// 			// 	array_push($session_data, ['line_items' => []]);
// 			// }
// 			// if (!isset($session_data['mode'])) {
// 			// 	$session_data['mode'] = 'subscription';
// 			// }

// 			// foreach ($session_data['subscription_data']['items'] as $k => $subscription_item) {
// 			// 	$plan = $subscription_item['plan'];
// 			// 	// Parse amount from plan. We cannot use plan + currency together.
// 			// 	$parse = explode('_', $plan);
// 			// 	$amount = array_pop($parse);

// 			// 	$session_data['line_items'][] = [
// 			// 		'quantity' => 1,
// 			// 		// 'price'		 => $subscription_item['plan'],
// 			// 		'amount'	 => $amount,
// 			// 		'currency' => $currency,
// 			// 	];
// 			// }

// 			// unset($session_data['subscription_data']); 
// 		}
// 	}

// 	// GFCommon::log_debug('gform_is_value_match: $rule => ' . print_r($submission_data, 1));
// 	return $session_data;
// }, 10, 5);

// 2. Modify form.currency value
// add_action('gform_pre_submission', 'lb_pre_submission_handler');
// function lb_pre_submission_handler($form) {
// 	$currency = isset($_POST['input_10']) ? $_POST['input_10'] : '';

// 	if ($currency) {
// 		$form['currency'] = $currency;
// 	}
// }
add_filter('gform_currency_pre_save_entry', function ($currency, $form) {
	// GFCommon::log_debug('gform_is_value_match: $rule => ' . print_r($currency, 1));
	// GFCommon::log_debug('gform_is_value_match: $rule => ' . print_r(rgpost('input_10'), 1));
	$curr = rgpost('input_10');
	// return rgpost('input_10');
	return $curr;
}, 10, 2);

add_filter('gform_currencies', function ($currencies) {
	// Set decimals allowed for USD to 0.
	$currencies['USD']['decimals'] = 0;
	$currencies['GBP']['decimals'] = 0;
	return $currencies;
});

function lb_latest_post_func($atts) {
	$atts = shortcode_atts(array(
		'type' => 'image_in_box',
		'order_by' => 'date',
		'order' => 'DESC',
		'title_tag' => 'h2',
		'display_category' => 1,
		'display_comments' => 0,
		'display_like' => 0,
		'display_share' => 0,
		'number_of_posts' => 1,
		'text_length' => 0,
		'category' => ''
	), $atts);

	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => $atts['number_of_posts'],
		'order'	=> $atts['order'],
		'orderby' => $atts['order_by'],
		'category_name' => $atts['category'],
	);

	$q = new WP_Query($args);

	$type = $atts['type'];
	$columns_number = '';
	$padding_latest_post = '';
	$title_tag = $atts['title_tag'];
	$display_time = 1;
	$display_like = $atts['display_like'];
	$display_share = $atts['display_share'];
	$display_comments = $atts['display_comments'];
	$display_category = $atts['display_category'];
	$blog_hide_comments = 'yes';
	$qode_like = 'off';

	ob_start();
?>
	<div class='latest_post_holder <?php echo esc_attr($type) . ' ' . esc_attr($columns_number) . ' ' . esc_attr($rows_number); ?>'>
		<ul>
			<?php while ($q->have_posts()) : $q->the_post();
				$cat = get_the_category(); ?>

				<li class="clearfix">
					<div class="latest_post" <?php echo esc_attr($padding_latest_post); ?>>
						<div class="latest_post_image clearfix">
							<a itemprop="url" href="<?php echo get_permalink(); ?>">
								<?php $featured_image_array = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large'); ?>
								<img itemprop="image" src="<?php echo esc_url($featured_image_array[0]); ?>" alt="" />
							</a>
						</div>


						<div class="latest_post_text">
							<div class="latest_post_inner">
								<div class="latest_post_text_inner">
									<<?php echo esc_attr($title_tag); ?> itemprop="name" class="latest_post_title entry_title"><a itemprop="url" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></<?php echo esc_attr($title_tag); ?>>
									<?php // echo wp_kses_post($this_object->getExcerpt(get_the_ID(), $params['text_length'])); 
									?>

									<span class="post_infos">
										<?php if ($display_time == '1') { ?>
											<span class="date_hour_holder">
												<span itemprop="dateCreated" class="date entry_date updated"><?php echo get_the_time('d F, Y'); ?>
													<meta itemprop="interactionCount" content="UserComments: <?php echo get_comments_number(bridge_qode_get_page_id()); ?>" />
												</span>
											</span>
										<?php } ?>

										<?php if ($display_category == '1') { ?>
											<span class="dots"><i class="fa fa-square"></i></span>
											<?php foreach ($cat as $categ) { ?>
												<a itemprop="url" href="<?php echo get_category_link($categ->term_id); ?>"><?php echo esc_attr($categ->cat_name); ?></a>
										<?php }
										} ?>

										<?php if ($blog_hide_comments != "yes" && $display_comments == "1") {
											$comments_count = get_comments_number();
											switch ($comments_count) {
												case 0:
													$comments_count_text = esc_html__('No comment', 'bridge-core');
													break;
												case 1:
													$comments_count_text = $comments_count . ' ' . esc_html__('Comment', 'bridge-core');
													break;
												default:
													$comments_count_text = $comments_count . ' ' . esc_html__('Comments', 'bridge-core');
													break;
											} ?>

											<span class="dots"><i class="fa fa-square"></i></span>
											<a itemprop="url" class="post_comments" href="<?php echo get_comments_link(); ?>"><?php echo esc_attr($comments_count_text); ?></a>
										<?php } ?>

										<?php if ($qode_like == "on" && function_exists('bridge_core_like') && $display_like == '1') { ?>
											<span class="dots"><i class="fa fa-square"></i></span>
											<span class="blog_like"><?php echo bridge_core_like_latest_posts(); ?></span>
										<?php } ?>

										<?php if ($display_share == '1') { ?>
											<span class="dots"><i class="fa fa-square"></i></span>
											<?php echo do_shortcode('[social_share]'); ?>
										<?php } ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</li>
			<?php
			endwhile;
			wp_reset_postdata();
			?>

		</ul>
	</div>
<?php
	return ob_get_clean();
}
add_shortcode('lb_latest_post', 'lb_latest_post_func');

/**
 * Expose ACF to Share to Care post type
 */

function acf_to_rest_api($response, $post, $request) {
	if (!function_exists('get_fields')) return $response;

	if (isset($post)) {
		$acf = get_fields($post->id);
		$response->data['acf'] = $acf;
	}
	return $response;
}

add_filter('rest_prepare_devotional_image', 'acf_to_rest_api', 10, 3);

/**
 * Relevanssi + REST API
 */

/**
 * Add custom fields to Relevanssi search on Share to Care
 */

add_filter('relevanssi_content_to_index', 'rlv_index_date', 10, 2);
function rlv_index_date($content, $post) {
	$content .= get_field('title_it', $post->ID) ? ' ' . get_field('title_it', $post->ID) : '';

	$date = get_the_date('j F Y', $post->ID);
	switch_to_locale('it_IT');
	$date .= ' ' . get_the_date('F', $post->ID);

	return $content . ' ' . $date;
}

add_action('rest_api_init', 'relevanssi_rest_api_filter_add_filters');

/**
 * Custom Relevanssi Endpoint
 * Required for Relevanssi PRO plugin
 */

// Add filter to posts
function relevanssi_rest_api_filter_add_filters() {

	// Register new route for search queries
	register_rest_route('relevanssi/v1', 'search', array(
		'methods'             => WP_REST_Server::READABLE,
		'callback'            => 'relevanssi_search_route_callback',
		'permission_callback' => '__return_true',
	));
}

/**
 * Generate results for the /wp-json/relevanssi/v1/search route.
 *
 * @param WP_REST_Request $request Full details about the request.
 *
 * @return WP_REST_Response|WP_Error The response for the request.
 */
function relevanssi_search_route_callback(WP_REST_Request $request) {

	$parameters = $request->get_query_params();

	// Force the posts_per_page to be no more than 10
	if (isset($parameters['per_page']) && ((int) $parameters['per_page'] >= 1 && (int) $parameters['per_page'] <= 10)) {
		$per_page = intval($parameters['per_page']);
	} else {
		$per_page = 10;
	}

	// default search args
	$args = array(
		's'               => $parameters['s'],
		'posts_per_page'  => $per_page,
		'post_type'     	=> $parameters['post_type'],
		'paged'       		=> $parameters['page'],
	);

	if (isset($parameters['devotional_image_category'])) {
		$args['tax_query'] = array(
			array(
				'taxonomy'   => 'devotional_image_category',
				'field'   => 'term_id',
				'terms'   => $parameters['devotional_image_category'],
			)
		);
	}

	// run query
	$search_query = new WP_Query();
	$search_query->parse_query($args);
	if (function_exists('relevanssi_do_query')) {
		relevanssi_do_query($search_query);
	}

	$controller = new WP_REST_Posts_Controller('devotional_image');
	$posts = array();

	$total_posts = $search_query->found_posts;
	$max_pages = $search_query->max_num_pages;

	while ($search_query->have_posts()) : $search_query->the_post();
		$data    = $controller->prepare_item_for_response($search_query->post, $request);
		$posts[] = $controller->prepare_response_for_collection($data);
	endwhile;

	// return results
	if (!empty($posts)) {
		$response = new WP_REST_Response($posts, 200);
		$response->header('X-WP-Total', $total_posts);
		$response->header('X-WP-TotalPages', $max_pages);
		return $response;
	} else {
		$response = new WP_REST_Response($posts, 200);
		return $response;
		// return new WP_Error( 'No results', 'Nothing found' );
	}
}

function remove_google_fonts(){ 
    wp_dequeue_style('bridge-style-handle-google-fonts');
    wp_deregister_style('bridge-style-handle-google-fonts');
}

add_action('wp_enqueue_scripts', 'remove_google_fonts', 100);
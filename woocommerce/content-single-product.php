<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

global $product;

?>

<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked wc_print_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
	echo get_the_password_form();
	return;
}
?>
<?php
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/', $url);
$acceptLang = ['it', 'en'];
$lang = in_array($parts[1], $acceptLang) ? $parts[1] : 'en';
?>

<?php if ($lang == 'en') { ?>

	<div class="all_publications_wrapper">
		<div class="all_publications"><a href="/en/news-events/publications/"><svg xmlns="http://www.w3.org/2000/svg" width="18.037" height="18.037" viewBox="0 0 18.037 18.037">
					<path id="Path_3922" data-name="Path 3922" d="M22.037,11.891H8.318l6.3-6.3L13.019,4,4,13.019l9.019,9.019,1.59-1.59-6.29-6.3H22.037Z" transform="translate(-4 -4)" fill="#b47b28"></path>
				</svg> All Publications</a></div>
		<div class="desktop_hide"> <?php do_action('woocommerce_share'); ?>

		<?php } else if ($lang == 'it') { ?>

			<div class="all_publications_wrapper">
				<div class="all_publications"><a href="/it/notizie-eventi/pubblicazioni/"><svg xmlns="http://www.w3.org/2000/svg" width="18.037" height="18.037" viewBox="0 0 18.037 18.037">
							<path id="Path_3922" data-name="Path 3922" d="M22.037,11.891H8.318l6.3-6.3L13.019,4,4,13.019l9.019,9.019,1.59-1.59-6.29-6.3H22.037Z" transform="translate(-4 -4)" fill="#b47b28"></path>
						</svg> Tutte le pubblicazioni</a></div>
				<div class="desktop_hide"> <?php do_action('woocommerce_share'); ?>

					<?php
					function translate_share_on_text($translated_text) {
						if ($translated_text == 'Share on: ') {
							$translated_text = 'Condividere su: ';
						}
						return $translated_text;
					}
					add_filter('gettext', 'translate_share_on_text', 20);
					?>

				<?php } else {
			} ?>

				<?php do_action('woocommerce_product_meta_end'); ?></div>
			</div>
			<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action('woocommerce_before_single_product_summary');
				?>

				<div class="summary entry-summary">
					<div class="clearfix">
						<?php
						/**
						 * woocommerce_single_product_summary hook
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 */
						do_action('woocommerce_single_product_summary');
						?>
					</div><!-- .clearfix -->
				</div><!-- .summary -->

				<div class="desktop_hide read_more info">
					<div class="hidden-content"><?php the_content(); ?></div>
				</div>

				<?php
				/**
				 * woocommerce_after_single_product_summary hook
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action('woocommerce_after_single_product_summary');
				?>

				<meta itemprop="url" content="<?php the_permalink(); ?>" />

			</div><!-- #product-<?php the_ID(); ?> -->

			<?php do_action('woocommerce_after_single_product'); ?>
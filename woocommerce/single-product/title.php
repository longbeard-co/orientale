<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

?>

<div class="product_pretitle">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
	
	<?php do_action( 'woocommerce_share' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>

<div class="product_info">
	<?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
	
	<div class="book_formats">
		
		<?php if ( have_rows('book_formats') ) : ?>
		
		<?php while ( have_rows('book_formats') ) : the_row(); ?>
		<a class="book_format" href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('format'); ?><?php if ( get_sub_field('price') ) { ?><?php echo ' - '; ?><?php } ?><?php the_sub_field('price'); ?></a>
		<?php endwhile; ?>
		<?php else : ?>
		<?php endif; ?>
		
		
	</div>

	<div class="tablet_hide"><?php the_content(); ?></div>
</div>

<?php